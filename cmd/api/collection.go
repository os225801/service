package main

import (
	db "be-omnistroke/db/sqlc"
	"be-omnistroke/internal/pkg/dto/payload"
	"be-omnistroke/internal/pkg/dto/payload/request"
	"be-omnistroke/internal/pkg/enum"
	"be-omnistroke/internal/pkg/validator"
	"context"
	"fmt"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgtype"
	"net/http"
	"strconv"
)

const (
	CannotRemovePostFromCollection = "cannot remove post from collection: %w"
	CannotDeleteCollection         = "cannot delete collection: %w"

	CollectionNotFound = "collection not found"

	FailureToUpdateCollectionPost = "failed to update collection post: %w"

	UnauthorizedToDeleteCollection = "unauthorized to delete this collection"
	UnauthorizedToUpdateCollection = "unauthorized to update this collection"
)

func (app *application) createCollectionHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	var req request.CreateCollectionRequest
	err = app.readJson(w, r, &req)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	v := validator.New()
	if request.ValidateCreateCollectionRequest(v, req); !v.Valid() {
		app.failedValidationResponse(w, v.Errors)
		return
	}

	user, err := app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	collectionId, err := app.queries.CreateCollection(r.Context(), db.CreateCollectionParams{
		ID:   uuid.New(),
		Name: req.Data.Name,
		Description: pgtype.Text{
			String: req.Data.Description,
			Valid:  true,
		},
		UserID: user.ID,
	})
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data: map[string]uuid.UUID{
			"collectionId": collectionId,
		},
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) deleteCollectionHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	collectionId, err := app.readIDParam(r, "collectionId")
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	collection, err := app.queries.GetCollectionById(r.Context(), collectionId)
	if err != nil {
		app.notFoundErrorResponse(w, CollectionNotFound)
		return
	}

	user, err := app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	if collection.UserID != user.ID {
		app.unauthorizedErrorResponse(w, UnauthorizedToDeleteCollection)
		return
	}

	deletedCollection := db.DeleteCollectionParams{
		ID:     collectionId,
		UserID: user.ID,
	}

	err = app.deleteCollectionTransaction(r.Context(), deletedCollection)
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          nil,
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}

}

func (app *application) updateCollectionPostHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	var req request.UpdateCollectionPostRequest
	err = app.readJson(w, r, &req)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	v := validator.New()
	if request.ValidateUpdateCollectionPostRequest(v, req); !v.Valid() {
		app.failedValidationResponse(w, v.Errors)
		return
	}

	collectionId, err := app.readIDParam(r, "collectionId")
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	user, err := app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	collection, err := app.queries.GetCollectionById(r.Context(), collectionId)
	if err != nil {
		app.notFoundErrorResponse(w, CollectionNotFound)
		return
	}

	if collection.UserID != user.ID {
		app.unauthorizedErrorResponse(w, UnauthorizedToUpdateCollection)
		return
	}

	postId, _ := uuid.Parse(req.Data.PostId)

	err = app.updateCollectionPostTransaction(r.Context(), collection, postId)
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	isExisted, err := app.queries.CheckPostInCollection(r.Context(), db.CheckPostInCollectionParams{
		CollectionID: collection.ID,
		PostID:       postId,
	})

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data: map[string]bool{
			"isExisted": isExisted,
		},
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) updateCollectionInfoHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	collectionId, err := app.readIDParam(r, "collectionId")
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	if isCollectionExisted, err := app.queries.IsCollectionExistedById(r.Context(), collectionId); err != nil || !isCollectionExisted {
		app.notFoundErrorResponse(w, PostNotFound)
		return
	}

	var req request.UpdateCollectionInfoRequest
	err = app.readJson(w, r, &req)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	user, err := app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	collection, err := app.queries.GetCollectionById(r.Context(), collectionId)
	if err != nil {
		app.notFoundErrorResponse(w, CollectionNotFound)
		return
	}

	if collection.UserID != user.ID {
		app.unauthorizedErrorResponse(w, UnauthorizedToUpdateCollection)
		return
	}

	err = app.queries.UpdateCollectionInfo(r.Context(), db.UpdateCollectionInfoParams{
		Name: defaultIfEmptyString(req.Data.Name, collection.Name),
		Description: pgtype.Text{
			String: req.Data.Description,
			Valid:  true,
		},
		ID:     collectionId,
		UserID: user.ID,
	})
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          nil,
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) getSelfCollectionsHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	user, err := app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	postId, err := app.readIDParam(r, "postId")
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}
	if isPostExisted, err := app.queries.IsPostExistedById(r.Context(), postId); err != nil || !isPostExisted {
		app.notFoundErrorResponse(w, PostNotFound)
		return
	}

	collections, err := app.queries.GetCollectionsByCurrentUser(r.Context(), db.GetCollectionsByCurrentUserParams{
		PostID: postId,
		UserID: user.ID,
	})
	if err != nil {
		app.notFoundErrorResponse(w, CollectionNotFound)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data: map[string]interface{}{
			"items": collections,
		},
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) getCollectionsPaginatedHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	userId, err := app.readIDParam(r, "userId")
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	pageNumber, err := strconv.Atoi(app.readStrings(r.URL.Query(), "page", "1"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	pageSize, err := strconv.Atoi(app.readStrings(r.URL.Query(), "size", "10"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	if isUserExisted, err := app.queries.IsUserExistedById(r.Context(), userId); err != nil || !isUserExisted {
		app.notFoundErrorResponse(w, UserNotFound)
		return
	}

	collections, err := app.queries.GetCollectionsPaginated(r.Context(), db.GetCollectionsPaginatedParams{
		Size:   int32(pageSize),
		Page:   int32(pageNumber),
		UserID: userId,
	})
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          collections,
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) getCollectionDetailsHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
	}

	collectionId, err := app.readIDParam(r, "collectionId")
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	collection, err := app.queries.GetCollectionDetails(r.Context(), collectionId)
	if err != nil {
		app.notFoundErrorResponse(w, CollectionNotFound)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          collection,
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) updateCollectionPostTransaction(ctx context.Context, collection db.GetCollectionByIdRow,
	postId uuid.UUID) error {

	tx, err := app.dbPool.Begin(ctx)
	if err != nil {
		return fmt.Errorf(FailureToBeginDBTransaction)
	}
	defer func(tx pgx.Tx, ctx context.Context) {
		_ = tx.Rollback(ctx)
	}(tx, ctx)

	qtx := app.queries.WithTx(tx)

	isExisted, err := qtx.CheckPostInCollection(ctx, db.CheckPostInCollectionParams{
		CollectionID: collection.ID,
		PostID:       postId,
	})
	if err != nil {
		return err
	}

	if !isExisted {
		err = app.queries.AddPostToCollection(ctx, db.AddPostToCollectionParams{
			CollectionID: collection.ID,
			PostID:       postId,
		})
	} else {
		err = app.queries.RemovePostFromCollection(ctx, db.RemovePostFromCollectionParams{
			CollectionID: collection.ID,
			PostID:       postId,
		})
	}
	if err != nil {
		return fmt.Errorf(FailureToUpdateCollectionPost, err)
	}

	if err = app.commitDBTransaction(ctx, tx); err != nil {
		return fmt.Errorf(FailureToCommitDBTransaction)
	}

	return nil
}

func (app *application) deleteCollectionTransaction(ctx context.Context, deletedCollection db.DeleteCollectionParams) error {
	tx, err := app.dbPool.Begin(ctx)
	if err != nil {
		return fmt.Errorf(FailureToBeginDBTransaction)
	}
	defer func(tx pgx.Tx, ctx context.Context) {
		_ = tx.Rollback(ctx)
	}(tx, ctx)

	qtx := app.queries.WithTx(tx)

	err = qtx.RemoveAllPostsFromCollection(ctx, deletedCollection.ID)
	if err != nil {
		return fmt.Errorf(CannotRemovePostFromCollection, err)
	}

	if err = qtx.DeleteCollection(ctx, deletedCollection); err != nil {
		return fmt.Errorf(CannotDeleteCollection, err)
	}

	if err = app.commitDBTransaction(ctx, tx); err != nil {
		return fmt.Errorf(FailureToCommitDBTransaction)
	}

	return nil
}
