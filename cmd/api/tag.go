package main

import (
	db "be-omnistroke/db/sqlc"
	"be-omnistroke/internal/pkg/dto/payload"
	"be-omnistroke/internal/pkg/dto/payload/request"
	"be-omnistroke/internal/pkg/enum"
	"be-omnistroke/internal/pkg/validator"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgtype"
	"net/http"
	"strconv"
)

const (
	TagNotFound  = "tag not found!"
	TagsNotFound = "one or more tags not found"
	TagExisted   = "tag already existed"
)

func (app *application) getTagsPaginatedHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	pageNumber, err := strconv.Atoi(app.readStrings(r.URL.Query(), "page", "1"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	pageSize, err := strconv.Atoi(app.readStrings(r.URL.Query(), "size", "10"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	searchTerm := app.readStrings(r.URL.Query(), "searchTerm", "")

	tags, err := app.queries.GetTagsPaginated(r.Context(), db.GetTagsPaginatedParams{
		Size:       int32(pageSize),
		Page:       int32(pageNumber),
		SearchTerm: searchTerm,
	})
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          tags,
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) getTagByIdHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	tagId, err := app.readIDParam(r, "tagId")
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	tag, err := app.queries.GetTagById(r.Context(), tagId)
	if err != nil {
		app.notFoundErrorResponse(w, TagNotFound)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          tag,
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) createTagHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	var req request.CreateTagRequest
	err = app.readJson(w, r, &req)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
	}

	v := validator.New()
	if request.ValidateCreateTagRequest(v, req); !v.Valid() {
		app.failedValidationResponse(w, v.Errors)
		return
	}

	if isTagExisted, err := app.queries.CheckTagExistsByName(r.Context(), req.Data.TagName); err != nil || isTagExisted {
		app.badRequestErrorResponse(w, TagExisted)
		return
	}

	tagId, err := app.queries.CreateTag(r.Context(), db.CreateTagParams{
		ID:      uuid.New(),
		TagName: req.Data.TagName,
		TagDescription: pgtype.Text{
			String: req.Data.Description,
			Valid:  true,
		},
	})
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusCreated, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data: map[string]string{
			"tagId": tagId.String(),
		},
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}
