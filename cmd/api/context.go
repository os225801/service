package main

import (
	"be-omnistroke/internal/pkg/enum"
	"context"
	"fmt"
	"net/http"
)

type contextKey string

const tokenContextKey = contextKey(enum.Token)

func (app *application) contextSetToken(r *http.Request, tokenString string) *http.Request {
	ctx := context.WithValue(r.Context(), tokenContextKey, tokenString)
	return r.WithContext(ctx)
}

func (app *application) contextGetToken(r *http.Request) (string, error) {
	token, ok := r.Context().Value(tokenContextKey).(string)
	if token == "" || !ok {
		return "", fmt.Errorf(enum.UnauthorizedMessage)
	}

	return token, nil
}
