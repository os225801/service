package main

import (
	db "be-omnistroke/db/sqlc"
	"be-omnistroke/internal/pkg/dto/payload"
	"be-omnistroke/internal/pkg/dto/payload/request"
	"be-omnistroke/internal/pkg/enum"
	"be-omnistroke/internal/pkg/token"
	"be-omnistroke/internal/pkg/validator"
	"context"
	"fmt"
	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgtype"
	"net/http"
	"time"
)

const (
	FailureToCreateSession = "failed to create session: %w"
	SessionDoesNotExist    = "session does not exist"
)

func (app *application) token(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	claims := token.Claims{
		RegisteredClaims: jwt.RegisteredClaims{
			Subject:   "",
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(enum.ApiTokenExpiredTime)),
			NotBefore: jwt.NewNumericDate(time.Now()),
			IssuedAt:  jwt.NewNumericDate(time.Now()),
		},
	}

	tkn, err := claims.CreateToken(app.config.secretKey)
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data: map[string]interface{}{
			enum.Token: tkn,
		},
	}, nil)

	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) syncUser(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	var req request.SyncUserRequest
	err = app.readJson(w, r, &req)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	v := validator.New()
	if request.ValidateSyncUserRequest(v, req); !v.Valid() {
		app.failedValidationResponse(w, v.Errors)
		return
	}

	expiryDate, err := time.Parse(time.RFC3339, req.Data.ExpiryDate)
	if err != nil {
		app.badRequestErrorResponse(w, "wrong time format")
		return
	}

	claims := token.Claims{
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(expiryDate),
			NotBefore: jwt.NewNumericDate(time.Now()),
			IssuedAt:  jwt.NewNumericDate(time.Now()),
		},
	}

	session, err := app.queries.GetSessionByUserId(r.Context(), req.Data.AuthId)
	if err != nil {
		// First time user
		userCreationParams := db.CreateUserParams{
			ID:         uuid.New(),
			Username:   req.Data.Username,
			AvatarPath: app.config.defaultAvatar,
			BannerPath: app.config.defaultBanner,
			AvatarLink: app.createImageLink(app.config.defaultAvatar).String,
			BannerLink: app.createImageLink(app.config.defaultBanner).String,
			Name:       req.Data.Name,
		}

		sessionCreationParams := db.CreateSessionParams{
			ID: req.Data.AuthId,
			Token: pgtype.Text{
				String: req.Data.Token,
				Valid:  true,
			},
			ExpiryDate: pgtype.Timestamp{
				Time:  expiryDate,
				Valid: true,
			},
			CreatedAt: pgtype.Timestamp{
				Time:  time.Now(),
				Valid: true,
			},
			RoleID: pgtype.Int4{
				Int32: int32(req.Data.Role),
				Valid: true,
			},
			UserID: userCreationParams.ID,
		}

		walletCreationParams := db.CreateWalletParams{
			ID:     uuid.New(),
			UserID: userCreationParams.ID,
		}

		err = app.createFirstTimeUser(r.Context(), userCreationParams, sessionCreationParams, walletCreationParams)
		if err != nil {
			app.serverErrorResponse(w, err)
			return
		}

		claims.RegisteredClaims.Subject = userCreationParams.ID.String()

		tkn, err := claims.CreateToken(app.config.secretKey)
		if err != nil {
			app.serverErrorResponse(w, err)
			return
		}

		err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
			ResultCode:    enum.SuccessCode,
			ResultMessage: enum.SuccessMessage,
			Data: map[string]string{
				enum.Token: tkn,
			},
		}, nil)

		if err != nil {
			app.serverErrorResponse(w, err)
			return
		}
		return
	}

	updateSessionParams := db.UpdateSessionParams{
		Token: pgtype.Text{
			String: req.Data.Token,
			Valid:  true,
		},
		RoleID: pgtype.Int4{
			Int32: int32(req.Data.Role),
			Valid: true,
		},
		ExpiryDate: pgtype.Timestamp{
			Time:  expiryDate,
			Valid: true,
		},
		ID: session.ID,
	}

	err = app.queries.UpdateSession(r.Context(), updateSessionParams)
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	claims.RegisteredClaims.Subject = session.UserID.String()

	tkn, err := claims.CreateToken(app.config.secretKey)
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data: map[string]string{
			enum.Token: tkn,
		},
	}, nil)

	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}
}

func (app *application) invalidateSession(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	var req request.InvalidateSessionRequest
	err = app.readJson(w, r, &req)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	v := validator.New()
	if request.ValidateInvalidateSessionRequest(v, req); !v.Valid() {
		app.failedValidationResponse(w, v.Errors)
		return
	}

	session, err := app.queries.GetSessionByUserId(r.Context(), req.UserId)
	if err != nil {
		app.notFoundErrorResponse(w, SessionDoesNotExist)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data: map[string]string{
			enum.Token: session.Token.String,
		},
	}, nil)

	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) createFirstTimeUser(ctx context.Context, userParams db.CreateUserParams,
	sessionParams db.CreateSessionParams, walletParams db.CreateWalletParams) error {
	tx, err := app.dbPool.Begin(ctx)
	if err != nil {
		return fmt.Errorf(FailureToBeginDBTransaction)
	}

	defer func(tx pgx.Tx, ctx context.Context) {
		_ = tx.Rollback(ctx)
	}(tx, ctx)

	qtx := app.queries.WithTx(tx)

	err = qtx.CreateUser(ctx, userParams)
	if err != nil {
		return fmt.Errorf(FailureToCreateUser, err)
	}

	err = qtx.CreateSession(ctx, sessionParams)
	if err != nil {
		return fmt.Errorf(FailureToCreateSession, err)
	}

	err = qtx.CreateWallet(ctx, walletParams)
	if err != nil {
		return fmt.Errorf(FailureToCreateWallet, err)
	}

	err = tx.Commit(ctx)
	if err != nil {
		return fmt.Errorf(FailureToCommitDBTransaction)
	}

	return nil
}
