package main

import (
	"be-omnistroke/internal/pkg/dto/payload"
	"be-omnistroke/internal/pkg/enum"
	"net/http"
)

func (app *application) routes() http.Handler {
	routes := http.NewServeMux()

	// Root routes
	routes.HandleFunc("GET /hello-world", func(writer http.ResponseWriter, request *http.Request) {
		writer.Header().Set("Content-Type", "application/json")
		response := payload.BaseResponse{
			ResultCode:    enum.SuccessCode,
			ResultMessage: "Hello world from omni-stroke",
			Data:          nil,
		}

		err := app.writeJson(writer, 200, response, nil)
		if err != nil {
			app.serverErrorResponse(writer, err)
		}
	})

	routes.HandleFunc("GET /health-check", func(writer http.ResponseWriter, request *http.Request) {
		ctx := request.Context()
		err := app.dbPool.Ping(ctx)
		response := payload.BaseResponse{
			ResultCode:    enum.SuccessCode,
			ResultMessage: enum.SuccessMessage,
			Data:          nil,
		}

		if err != nil {
			response.ResultCode = enum.SystemErrorCode
			response.ResultMessage = enum.SystemErrorMessage
			app.serverErrorResponse(writer, err)
			return
		}

		err = app.writeJson(writer, 200, response, nil)
		if err != nil {
			app.serverErrorResponse(writer, err)
		}
	})

	// Token routes
	routes.HandleFunc("POST /token", app.token)
	routes.HandleFunc("POST /user/sync", app.requireAuthentication(app.syncUser))
	routes.HandleFunc("POST /session/invalidate", app.requireAuthentication(app.invalidateSession))

	// Artwork routes
	routes.HandleFunc("POST /artworks", app.requireAuthentication(app.uploadArtworkHandler))
	routes.HandleFunc("POST /artworks/v2", app.uploadArtworkV2Handler)
	routes.HandleFunc("DELETE /artworks/{artworkId}", app.requireAuthentication(app.deleteArtworkHandler))
	routes.HandleFunc("GET /artworks/{artworkId}/download", app.requireAuthentication(app.downloadArtworkHandler))

	// Price routes

	// Post routes
	routes.HandleFunc("POST /posts", app.requireAuthentication(app.createPostHandler))
	routes.HandleFunc("PUT /posts/{postId}", app.requireAuthentication(app.updatePostHandler))
	routes.HandleFunc("DELETE /posts/{postId}", app.requireAuthentication(app.deletePostHandler))
	routes.HandleFunc("GET /posts/{postId}", app.getPostInfoByIdHandler)
	routes.HandleFunc("GET /posts", app.searchPostsHandler)
	routes.HandleFunc("GET /posts/users/{userId}", app.getPostsByArtistHandler)
	routes.HandleFunc("POST /posts/{postId}/views", app.increasePostViewsHandler)
	routes.HandleFunc("GET /posts/collections/{collectionId}", app.getPostsByCollectionHandler)

	// Collection routes
	routes.HandleFunc("POST /collections", app.requireAuthentication(app.createCollectionHandler))
	routes.HandleFunc("DELETE /collections/{collectionId}", app.requireAuthentication(app.deleteCollectionHandler))
	routes.HandleFunc("PUT /collections/{collectionId}/posts", app.requireAuthentication(app.updateCollectionPostHandler))
	routes.HandleFunc("PUT /collections/{collectionId}", app.requireAuthentication(app.updateCollectionInfoHandler))
	routes.HandleFunc("GET /collections/posts/{postId}", app.requireAuthentication(app.getSelfCollectionsHandler))
	routes.HandleFunc("GET /collections/users/{userId}", app.getCollectionsPaginatedHandler)
	routes.HandleFunc("GET /collections/{collectionId}", app.getCollectionDetailsHandler)

	// Comment routes
	routes.HandleFunc("POST /comments", app.requireAuthentication(app.createCommentHandler))
	routes.HandleFunc("DELETE /comments/{commentId}", app.requireAuthentication(app.deleteCommentHandler))
	routes.HandleFunc("GET /comments/{commentId}", app.getCommentByIdHandler)
	routes.HandleFunc("GET /comments/posts/{postId}", app.getCommentsByPostHandler)
	routes.HandleFunc("GET /comments/parent-comments/{parentCommentId}", app.getCommentsByParentCommentHandler)
	routes.HandleFunc("GET /comments/{commentId}/sub-comments/count", app.countSubCommentsByIdHandler)

	// Like routes
	routes.HandleFunc("POST /posts/{postId}/likes", app.requireAuthentication(app.likePostHandler))

	// Tag routes
	routes.HandleFunc("GET /tags", app.getTagsPaginatedHandler)
	routes.HandleFunc("GET /tags/{tagId}", app.getTagByIdHandler)
	routes.HandleFunc("POST /tags", app.createTagHandler)

	// Transaction routes
	routes.HandleFunc("POST /transactions", app.requireAuthentication(app.withdrawMoneyHandler))
	routes.HandleFunc("PUT /transactions/{transactionId}", app.requireAuthentication(app.updateWithdrawMoneyStatusHandler))
	routes.HandleFunc("GET /transactions/{transactionId}", app.requireAuthentication(app.getTransactionByIdHandler))
	routes.HandleFunc("GET /users/transactions", app.requireAuthentication(app.getTransactionHistoryHandler))
	routes.HandleFunc("GET /transactions", app.requireAuthentication(app.getTransactionsPaginatedHandler))

	// Order routes
	routes.HandleFunc("POST /orders", app.requireAuthentication(app.createOrderHandler))
	routes.HandleFunc("GET /orders/{orderId}", app.requireAuthentication(app.getOrderByIdHandler))
	routes.HandleFunc("GET /users/orders", app.requireAuthentication(app.getOrdersHistoryHandler))
	routes.HandleFunc("GET /orders", app.requireAuthentication(app.getOrdersPaginatedHandler))
	routes.HandleFunc("GET /orders/cancel", app.cancelOrderHandler)
	routes.HandleFunc("GET /orders/success", app.updateOrderStatusHandler)

	// User routes
	routes.HandleFunc("PUT /users/info", app.requireAuthentication(app.updateUserInfoHandler))
	routes.HandleFunc("PUT /users/image", app.requireAuthentication(app.updateUserImageHandler))
	routes.HandleFunc("GET /users/{userId}", app.getUserProfileHandler)
	routes.HandleFunc("GET /users/current-user", app.requireAuthentication(app.getCurrentUserHandler))
	routes.HandleFunc("GET /users", app.requireAuthentication(app.getAllLoggedInUsersHandler))
	routes.HandleFunc("GET /users/count", app.requireAuthentication(app.countAllUsersHandler))

	// Follow routes
	routes.HandleFunc("POST /users/{userId}/follows", app.requireAuthentication(app.followUserHandler))

	// Wallet routes
	routes.HandleFunc("GET /wallets", app.requireAuthentication(app.getWalletHandler))

	return app.recoverPanic(app.cors(app.authenticate(routes)))
}
