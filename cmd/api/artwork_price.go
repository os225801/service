package main

const (
	PriceNotFound        = "price not found!"
	PriceSmallerThanZero = "price must be greater than 0"

	FailureToCreatePrice = "failed to create price: %w"
	FailureToUpdatePrice = "failed to update price: %w"
)
