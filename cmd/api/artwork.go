package main

import (
	db "be-omnistroke/db/sqlc"
	"be-omnistroke/internal/pkg/dto/payload"
	"be-omnistroke/internal/pkg/enum"
	"fmt"
	"github.com/google/uuid"
	"github.com/h2non/bimg"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"sync"
)

const (
	ArtworkNotFound       = "artwork not found!"
	ArtworkNotForSale     = "artwork is not for sale"
	ArtworkCannotDownload = "artwork cannot be downloaded"

	UnauthorizedToDeleteArtwork = "unauthorized to delete this artwork"

	Image          = "image"
	ProcessedImage = "processed image"

	FailureToDeleteArtwork = "failed to delete artwork"
	FailureToUpdateArtwork = "failed to update artwork: %w"

	WatermarkText = "OmniStroke (c)"
	WatermarkFont = "sans bold 12"
)

// Handlers
func (app *application) uploadArtworkHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	artist, err := app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	file, err := app.readFormFile(r, "image")
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	id := uuid.New()
	fileData, err := app.readFileBuffer(file)
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	imagePath, processedPath, err := saveImage(fileData, id.String(), artist.ID)
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	params := db.InsertArtworkParams{
		ID:                 id,
		ImagePath:          imagePath,
		ProcessedImagePath: processedPath,
		ImageLink:          app.createImageLink(imagePath).String,
		ProcessedImageLink: app.createImageLink(processedPath).String,
		Type:               0,
		ArtistID:           artist.ID,
	}

	artworkId, err := app.queries.InsertArtwork(r.Context(), params)
	if err != nil {
		delErr := app.deleteArtworkFile(imagePath, processedPath)
		if delErr != nil {
			app.serverErrorResponse(w, delErr)
			return
		}
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data: map[string]uuid.UUID{
			"artworkId": artworkId,
		},
	}, nil)

	if err != nil {
		delErr := app.deleteArtworkFile(imagePath, processedPath)
		if delErr != nil {
			app.serverErrorResponse(w, delErr)
			return
		}
		app.serverErrorResponse(w, err)
	}
}

func (app *application) uploadArtworkV2Handler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	artist, err := app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	userDirectory, err := getUserDirectory(artist.ID)
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	chunkIndex, err := strconv.Atoi(r.FormValue("chunkIndex"))
	if err != nil {
		app.badRequestErrorResponse(w, "invalid chunk index")
		return
	}

	totalChunks, err := strconv.Atoi(r.FormValue("totalChunks"))
	if err != nil {
		app.badRequestErrorResponse(w, "invalid total chunks")
		return
	}

	file, _, err := r.FormFile("chunk")
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}
	defer func(file multipart.File) {
		err = file.Close()
		if err != nil {
			app.serverErrorResponse(w, err)
		}
	}(file)

	chunkDir, err := app.addChunkToDirectory(artist.ID, chunkIndex, file, userDirectory)
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	if !checkAllChunksUploaded(artist.ID, totalChunks, chunkDir) {
		err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
			ResultCode:    enum.SuccessCode,
			ResultMessage: enum.SuccessMessage,
			Data:          nil,
		}, nil)
		if err != nil {
			app.serverErrorResponse(w, err)
		}
		return
	}

	artworkId := uuid.New()

	filePath, err := assembleAndRemoveChunks(artist.ID, totalChunks, artworkId, userDirectory, chunkDir)
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	imagePath, processedPath, err := addWatermarkAndSaveImage(filePath, userDirectory, artworkId.String())
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	params := db.InsertArtworkParams{
		ID:                 artworkId,
		ImagePath:          imagePath,
		ProcessedImagePath: processedPath,
		ImageLink:          app.createImageLink(imagePath).String,
		ProcessedImageLink: app.createImageLink(processedPath).String,
		Type:               0,
		ArtistID:           artist.ID,
	}

	artworkId, err = app.queries.InsertArtwork(r.Context(), params)
	if err != nil {
		delErr := app.deleteArtworkFile(imagePath, processedPath)
		if delErr != nil {
			app.serverErrorResponse(w, delErr)
			return
		}
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data: map[string]uuid.UUID{
			"artworkId": artworkId,
		},
	}, nil)

	if err != nil {
		delErr := app.deleteArtworkFile(imagePath, processedPath)
		if delErr != nil {
			app.serverErrorResponse(w, delErr)
			return
		}
		app.serverErrorResponse(w, err)
	}
}

func (app *application) deleteArtworkHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	artworkId, err := app.readIDParam(r, "artworkId")
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	user, err := app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	artwork, err := app.queries.GetArtworkById(r.Context(), artworkId)
	if err != nil {
		app.notFoundErrorResponse(w, ArtworkNotFound)
		return
	}

	if artwork.ArtistID != user.ID {
		app.unauthorizedErrorResponse(w, UnauthorizedToDeleteArtwork)
		return
	}

	if artwork.PostID != uuid.Nil {
		app.badRequestErrorResponse(w, ArtworkAlreadyHasPost)
		return
	}

	err = app.deleteArtworkFile(artwork.ImagePath, artwork.ProcessedImagePath)
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.queries.DeleteArtwork(r.Context(), db.DeleteArtworkParams{
		ID:       artworkId,
		ArtistID: user.ID,
	})
	if err != nil {
		app.serverErrorResponse(w, fmt.Errorf(FailureToDeleteArtwork))
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          nil,
	}, nil)

	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) downloadArtworkHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	artworkId, err := app.readIDParam(r, "artworkId")

	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	artwork, err := app.queries.GetArtworkById(r.Context(), artworkId)
	if err != nil {
		app.notFoundErrorResponse(w, ArtworkNotFound)
		return
	}

	user, err := app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	if artwork.IsBuyable && artwork.ArtistID != user.ID {
		isAllowed, err := app.queries.CheckUserDownloadedArtwork(r.Context(), db.CheckUserDownloadedArtworkParams{
			UserID:    user.ID,
			ArtworkID: artworkId,
		})
		if err != nil || !isAllowed {
			app.notFoundErrorResponse(w, ArtworkCannotDownload)
			return
		}
	}

	fileData, err := app.readFile(artwork.ImagePath)
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	filename := filepath.Base(artwork.ImagePath)

	err = app.writeOctetStream(w, fileData, filename)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

// Helper functions

func (app *application) addChunkToDirectory(fileID uuid.UUID, chunkIndex int, file multipart.File, uploadDir string) (string, error) {
	chunkDir := filepath.Join(uploadDir, enum.ChunkDirectory)
	_, err := os.Stat(chunkDir)
	if os.IsNotExist(err) {
		err = os.MkdirAll(chunkDir, 0777)
		if err != nil {
			return "", fmt.Errorf(enum.FailureToCreateDir, err)
		}
	}
	chunkFileName := fmt.Sprintf("%s_%d", fileID.String(), chunkIndex)
	chunkFilePath := filepath.Join(chunkDir, chunkFileName)
	chunkFile, err := os.OpenFile(chunkFilePath, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return "", fmt.Errorf(enum.FailureToOpenFile, err)
	}
	defer func(chunkFile multipart.File) {
		err = chunkFile.Close()
		if err != nil {
			return
		}
	}(chunkFile)

	_, err = io.Copy(chunkFile, file)
	if err != nil {
		return "", fmt.Errorf(enum.FailureToCopyFile, err)
	}
	return chunkDir, nil
}

func checkAllChunksUploaded(fileID uuid.UUID, totalChunks int, chunkDir string) bool {
	var wg sync.WaitGroup
	var mu sync.Mutex
	allChunksExist := true

	for i := 0; i < totalChunks; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			chunkFileName := fmt.Sprintf("%s_%d", fileID.String(), i)
			chunkFilePath := filepath.Join(chunkDir, chunkFileName)
			if _, err := os.Stat(chunkFilePath); os.IsNotExist(err) {
				mu.Lock()
				allChunksExist = false
				mu.Unlock()
			} else if err != nil {
				mu.Lock()
				allChunksExist = false
				mu.Unlock()
				fmt.Printf(enum.FailureToCheckFile, chunkFilePath, err)
			}
		}(i)
	}

	wg.Wait()
	return allChunksExist
}

func assembleAndRemoveChunks(fileID uuid.UUID, totalChunks int, artworkId uuid.UUID, userFolder, chunkDir string) (string, error) {
	outputFileName := fmt.Sprintf(enum.JpgFile, artworkId.String())
	outputFilePath := filepath.Join(userFolder, outputFileName)
	outputFile, err := os.OpenFile(outputFilePath, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return "", fmt.Errorf(enum.FailureToOpenFile, err)
	}
	defer func(outputFile *os.File) {
		err = outputFile.Close()
		if err != nil {
			return
		}
	}(outputFile)

	for i := 0; i < totalChunks; i++ {
		chunkFileName := fmt.Sprintf("%s_%d", fileID.String(), i)
		chunkFilePath := filepath.Join(chunkDir, chunkFileName)
		chunkFile, err := os.Open(chunkFilePath)
		if err != nil {
			return "", fmt.Errorf(enum.FailureToOpenFile, err)
		}

		_, err = io.Copy(outputFile, chunkFile)
		_ = chunkFile.Close()
		if err != nil {
			return "", fmt.Errorf(enum.FailureToCopyFile, err)
		}

		err = os.Remove(chunkFilePath)
		if err != nil {
			return "", fmt.Errorf(enum.FailureToDeleteFile, err)
		}
	}
	return outputFilePath, nil
}

func addWatermarkAndSaveImage(filePath string, userFolder string, fileName string) (string, string, error) {
	buffer, err := os.ReadFile(filePath)
	if err != nil {
		return "", "", fmt.Errorf(enum.FailureToReadFile, err)
	}
	watermark := bimg.Watermark{
		Text:       WatermarkText,
		Opacity:    0.25,
		Width:      200,
		DPI:        100,
		Margin:     150,
		Font:       WatermarkFont,
		Background: bimg.Color{R: 255, G: 255, B: 255},
	}

	processedImgType := bimg.ImageTypeName(bimg.WEBP)
	processedFileName := fmt.Sprintf("%s.%s", fileName, processedImgType)
	processedPath := filepath.Join(userFolder, processedFileName)
	imageType := bimg.DetermineImageType(buffer)
	typeName := bimg.ImageTypeName(imageType)
	fileName = fmt.Sprintf("o-%s.%s", fileName, typeName)

	path := filepath.Join(userFolder, fileName)

	image, err := bimg.NewImage(buffer).Convert(imageType)
	if err != nil {
		return "", "", fmt.Errorf(enum.FailureToCreateImage)
	}

	tempConvertedImage, err := bimg.NewImage(buffer).Convert(bimg.JPEG)
	if err != nil {
		return "", "", fmt.Errorf(enum.FailureToConvertImage, err)
	}

	watermarkImage, err := bimg.NewImage(tempConvertedImage).Watermark(watermark)
	if err != nil {
		return "", "", fmt.Errorf(enum.FailureToAddWatermark)
	}

	watermarkImage, err = bimg.NewImage(watermarkImage).Convert(bimg.WEBP)

	err = bimg.Write(path, image)
	if err != nil {
		return "", "", fmt.Errorf(enum.FailureToSaveImage, err)
	}

	err = bimg.Write(processedPath, watermarkImage)
	if err != nil {
		return "", "", fmt.Errorf(enum.FailureToSaveConvertedImage, err)
	}

	err = os.Remove(filePath)
	if err != nil {
		return "", "", fmt.Errorf(enum.FailureToDeleteFile, err)
	}

	return path, processedPath, nil
}

func saveImage(buffer []byte, fileName string, userId uuid.UUID) (string, string, error) {
	userFolder, err := getUserDirectory(userId)
	if err != nil {
		return "", "", err
	}

	watermark := bimg.Watermark{
		Text:       WatermarkText,
		Opacity:    0.25,
		Width:      200,
		DPI:        100,
		Margin:     150,
		Font:       WatermarkFont,
		Background: bimg.Color{R: 255, G: 255, B: 255},
	}

	processedImgType := bimg.ImageTypeName(bimg.WEBP)
	processedFileName := fmt.Sprintf("%s.%s", fileName, processedImgType)
	processedPath := filepath.Join(userFolder, processedFileName)
	imageType := bimg.DetermineImageType(buffer)
	typeName := bimg.ImageTypeName(imageType)
	fileName = fmt.Sprintf("o-%s.%s", fileName, typeName)

	path := filepath.Join(userFolder, fileName)

	image, err := bimg.NewImage(buffer).Convert(imageType)
	if err != nil {
		return "", "", fmt.Errorf(enum.FailureToCreateImage)
	}

	tempConvertedImage, err := bimg.NewImage(buffer).Convert(bimg.JPEG)
	if err != nil {
		return "", "", fmt.Errorf(enum.FailureToConvertImage, err)
	}

	watermarkImage, err := bimg.NewImage(tempConvertedImage).Watermark(watermark)
	if err != nil {
		return "", "", fmt.Errorf(enum.FailureToAddWatermark)
	}

	watermarkImage, err = bimg.NewImage(watermarkImage).Convert(bimg.WEBP)

	err = bimg.Write(path, image)
	if err != nil {
		return "", "", fmt.Errorf(enum.FailureToSaveImage, err)
	}

	err = bimg.Write(processedPath, watermarkImage)
	if err != nil {
		return "", "", fmt.Errorf(enum.FailureToSaveConvertedImage, err)
	}

	return path, processedPath, nil
}

func (app *application) deleteArtworkFile(imagePath, processedPath string) error {
	err := app.deleteImage(imagePath, Image)
	if err != nil {
		return err
	}

	err = app.deleteImage(processedPath, ProcessedImage)
	if err != nil {
		return err
	}
	return nil
}
