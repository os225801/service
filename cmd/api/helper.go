package main

import (
	db "be-omnistroke/db/sqlc"
	"be-omnistroke/internal/pkg/dto/payload"
	"be-omnistroke/internal/pkg/enum"
	"be-omnistroke/internal/pkg/token"
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgtype"
	"io"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
)

const (
	InvalidParameter          = "invalid %s parameter"
	InvalidRequestIdParameter = "invalid request id parameter"
	InvalidUUID               = "invalid uuid"

	DefaultImageDir = "uploads"

	RequestNotContainToken = "request does not contain token"

	FailureToBeginDBTransaction  = "failed to begin transaction"
	FailureToCommitDBTransaction = "failed to commit transaction"
)

var magicTable = map[string]string{
	"\xff\xd8\xff":      "image/jpeg",
	"\x89PNG\r\n\x1a\n": "image/png",
	"GIF87a":            "image/gif",
	"GIF89a":            "image/gif",
}

func (app *application) readIDParam(r *http.Request, key string) (uuid.UUID, error) {
	params := r.PathValue(key)

	id, err := uuid.Parse(params)
	if err != nil {
		return uuid.Nil, fmt.Errorf(InvalidParameter, strings.ToLower(key))
	}

	return id, nil
}

func (app *application) createImageLink(imagePath string) pgtype.Text {
	imageName := filepath.Base(imagePath)
	directory := filepath.Dir(imagePath)
	userDirectory := filepath.Base(directory)

	imageLink := app.config.imageDomain + userDirectory + "/" + imageName
	if strings.EqualFold(userDirectory, DefaultImageDir) {
		imageLink = app.config.imageDomain + imageName
	}

	return pgtype.Text{
		String: imageLink,
		Valid:  true,
	}
}

func (app *application) logRequest(r *http.Request) error {
	requestId := r.Header.Get(enum.HttpRequestHeader)
	_, err := uuid.Parse(requestId)
	if requestId == "" || err != nil {
		return errors.New(InvalidRequestIdParameter)
	}

	app.logger.PrintInfo("request: ", map[string]interface{}{
		"id":     requestId,
		"url":    r.URL.Path,
		"method": r.Method,
		"body":   r.Body,
	})
	return nil
}

func (app *application) logResponse(status int, data payload.BaseResponse) {
	app.logger.PrintInfo("response: ", map[string]interface{}{
		"status": status,
		"body":   data,
	})
}

func (app *application) writeOctetStream(writer http.ResponseWriter, data []byte, filename string) error {
	writer.Header().Set(enum.HttpContentType, enum.HttpOctetStream)
	writer.Header().Set(enum.HttpContentDisposition, fmt.Sprintf(enum.AttachmentFormat, filename))
	writer.WriteHeader(http.StatusOK)

	if _, err := writer.Write(data); err != nil {
		app.logger.PrintError(err, nil)
		return err
	}
	return nil
}

func (app *application) writeJson(writer http.ResponseWriter, status int, data payload.BaseResponse,
	headers http.Header) error {

	js, err := json.MarshalIndent(data, "", "\t")
	if err != nil {
		return err
	}

	for key, value := range headers {
		writer.Header()[key] = value
	}

	writer.Header().Set(enum.HttpContentType, enum.HttpJson)
	writer.WriteHeader(status)
	if _, err := writer.Write(js); err != nil {
		app.logger.PrintError(err, nil)
		return err
	}

	if status == http.StatusOK {
		app.logResponse(status, data)
	}
	return nil
}

func (app *application) readJson(writer http.ResponseWriter, request *http.Request, dst interface{}) error {
	maxBytes := 1_048_576
	request.Body = http.MaxBytesReader(writer, request.Body, int64(maxBytes))

	dec := json.NewDecoder(request.Body)
	dec.DisallowUnknownFields()

	err := dec.Decode(dst)
	if err != nil {
		var syntaxError *json.SyntaxError
		var unmarshalTypeError *json.UnmarshalTypeError
		var invalidUnmarshalError *json.InvalidUnmarshalError

		switch {

		case errors.As(err, &syntaxError):
			return fmt.Errorf(enum.BadlyFormedJsonAtCharacter, syntaxError.Offset)

		case errors.Is(err, io.ErrUnexpectedEOF):
			return errors.New(enum.BadlyFormedJson)

		case errors.As(err, &unmarshalTypeError):
			if unmarshalTypeError.Field != "" {
				return fmt.Errorf(enum.IncorrectJsonTypeForField,
					unmarshalTypeError.Field)
			}
			return fmt.Errorf(enum.IncorrectJsonTypeAtCharacter,
				unmarshalTypeError.Offset)

		case errors.Is(err, io.EOF):
			return errors.New(enum.BodyMustNotBeEmpty)

		case strings.HasPrefix(err.Error(), enum.JsonUnknownField):
			fieldName := strings.TrimPrefix(err.Error(), enum.JsonUnknownField)
			return fmt.Errorf(enum.BodyContainsUnknownKey, fieldName)

		case err.Error() == enum.RequestBodyTooLarge:
			return fmt.Errorf(enum.BodyTooLargeMessage, maxBytes)

		case errors.As(err, &invalidUnmarshalError):
			panic(err)

		default:
			return err
		}
	}

	err = dec.Decode(&struct{}{})
	if err != io.EOF {
		return errors.New(enum.BodyContainSingleJsonValue)
	}
	return nil
}

func (app *application) readStrings(qs url.Values, key string, defaultValue string) string {
	s := qs.Get(key)
	if s == "" {
		return defaultValue
	}

	return s
}

func (app *application) readFormFile(r *http.Request, key string) (*multipart.FileHeader, error) {

	_, file, err := r.FormFile(key)
	if err != nil {
		return nil, fmt.Errorf(enum.FailureToGetFile)
	}

	if file.Size == 0 {
		return nil, fmt.Errorf(enum.FileSizeCannotBeZero)
	}

	if file.Size > 50*1024*1024 {
		return nil, fmt.Errorf(enum.FileSizeTooLarge)
	}

	return file, nil
}

func (app *application) readFileBuffer(file *multipart.FileHeader) ([]byte, error) {
	fileData, err := file.Open()
	if err != nil {
		return nil, fmt.Errorf(enum.FailureToOpenFile, err)
	}
	defer func() {
		if cerr := fileData.Close(); cerr != nil && err == nil {
			err = cerr
		}
	}()

	buf := &bytes.Buffer{}
	_, err = io.Copy(buf, fileData)
	//buffer, err := io.ReadAll(fileData)
	if err != nil {
		return nil, fmt.Errorf(enum.FailureToReadBufferFile, err)
	}
	mime := mimeFromIncipit(buf.Bytes())
	if mime == "" {
		return nil, fmt.Errorf(enum.FileIsNotImage)
	}

	return buf.Bytes(), nil
}

func (app *application) deleteImage(path, fileType string) error {
	err := os.Remove(path)
	if err != nil {
		return fmt.Errorf(enum.FailureToDeleteFile, fileType)
	}

	return nil
}

func (app *application) readFile(filePath string) ([]byte, error) {
	fileData, err := os.ReadFile(filePath)
	if err != nil {
		return nil, fmt.Errorf(enum.FailureToReadFile, err)
	}

	return fileData, nil
}

func (app *application) getCurrentUser(r *http.Request) (*db.User, error) {
	tkn, err := app.contextGetToken(r)
	if err != nil {
		return nil, fmt.Errorf(RequestNotContainToken)
	}

	claims, err := token.GetClaims(tkn, app.config.secretKey)
	if err != nil {
		return nil, err
	}

	userId, err := uuid.Parse(claims.Subject)
	if err != nil {
		return nil, fmt.Errorf(InvalidUUID)
	}

	user, err := app.queries.GetUserById(r.Context(), userId)
	if err != nil {
		return nil, fmt.Errorf(UserNotFound)
	}

	return &user, nil
}

func (app *application) extractUserIdIfHasToken(r *http.Request) (*uuid.UUID, error) {
	user, err := app.getCurrentUser(r)
	if err != nil && err.Error() != RequestNotContainToken {
		return nil, err
	}
	if user != nil {
		return &user.ID, nil
	}
	return nil, nil
}

func mimeFromIncipit(incipit []byte) string {
	for magic, mime := range magicTable {
		if strings.HasPrefix(string(incipit), magic) {
			return mime
		}
	}
	return ""
}

func defaultIfEmptyString(value string, defaultValue string) string {
	if value == "" {
		return defaultValue
	}
	return value
}

func (app *application) commitDBTransaction(ctx context.Context, tx pgx.Tx) error {
	if err := tx.Commit(ctx); err != nil {
		return fmt.Errorf(FailureToCommitDBTransaction)
	}
	return nil
}

func getUserDirectory(userId uuid.UUID) (string, error) {
	projectDir, err := os.Getwd()
	if err != nil {
		return "", fmt.Errorf(enum.FailureToGetProjectDir, err)
	}

	userFolder := filepath.Join(projectDir, enum.ProjectDirectory, userId.String())
	_, err = os.Stat(userFolder)
	if os.IsNotExist(err) {
		err = os.MkdirAll(userFolder, 0777)
		if err != nil {
			return "", fmt.Errorf(enum.FailureToCreateDir, err)
		}
	}
	return userFolder, nil
}
