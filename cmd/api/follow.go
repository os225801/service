package main

import (
	db "be-omnistroke/db/sqlc"
	"be-omnistroke/internal/pkg/dto/payload"
	"be-omnistroke/internal/pkg/enum"
	"net/http"
)

const (
	CannotFollowYourself = "user cannot follow yourself"
)

func (app *application) followUserHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	userId, err := app.readIDParam(r, "userId")
	if err != nil {
		app.badRequestErrorResponse(w, UserIdErrorMessage)
		return
	}

	if isUserExist, err := app.queries.IsUserExistedById(r.Context(), userId); !isUserExist || err != nil {
		app.notFoundErrorResponse(w, UserNotFound)
		return
	}

	follower, err := app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	if follower.ID == userId {
		app.badRequestErrorResponse(w, CannotFollowYourself)
		return
	}

	followStatus, err := app.queries.CheckFollowStatus(r.Context(), db.CheckFollowStatusParams{
		UserID:     userId,
		FollowerID: follower.ID,
	})
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	if !followStatus {
		err = app.queries.FollowUser(r.Context(), db.FollowUserParams{
			UserID:     userId,
			FollowerID: follower.ID,
		})
	} else {
		err = app.queries.UnfollowUser(r.Context(), db.UnfollowUserParams{
			UserID:     userId,
			FollowerID: follower.ID,
		})
	}
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          nil,
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

//func (app *application) getFollowersByUser(w http.ResponseWriter, r *http.Request) {
//
//}

//func (app *application) getFollowingByUser(w http.ResponseWriter, r *http.Request) {
//
//}
