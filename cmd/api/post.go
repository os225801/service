package main

import (
	"be-omnistroke/db/sqlc"
	"be-omnistroke/internal/pkg/dto/payload"
	"be-omnistroke/internal/pkg/dto/payload/request"
	"be-omnistroke/internal/pkg/enum"
	"be-omnistroke/internal/pkg/validator"
	"context"
	"fmt"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgtype"
	"net/http"
	"strconv"
	"strings"
)

const (
	PostNotFound          = "post not found!"
	ArtworkAlreadyHasPost = "artwork already has post"

	FailureToCreatePost = "failed to create post: %w"
	FailureToUpdatePost = "failed to update post: %w"
	FailureToDeletePost = "failed to delete post: %w"

	FailureToCreatePostTags = "failed to create post tags: %w"
	FailureToDeletePostTags = "failed to delete post tags: %w"

	UnauthorizedToUpdatePost = "unauthorized to update this post"
	UnauthorizedToDeletePost = "unauthorized to delete this post"
)

func (app *application) createPostHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	var req request.CreatePostRequest
	err = app.readJson(w, r, &req)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	v := validator.New()
	if request.ValidateCreatePostRequest(v, req); !v.Valid() {
		app.failedValidationResponse(w, v.Errors)
		return
	}

	if req.Data.IsBuyable && req.Data.Price <= 0 {
		app.badRequestErrorResponse(w, PriceSmallerThanZero)
		return
	}

	if !req.Data.IsBuyable {
		req.Data.Price = 0
	}

	user, err := app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	artworkId, _ := uuid.Parse(req.Data.ArtworkId)

	artwork, err := app.queries.GetArtworkByIdAndArtist(r.Context(), db.GetArtworkByIdAndArtistParams{
		ID:       artworkId,
		ArtistID: user.ID,
	})
	if err != nil {
		app.notFoundErrorResponse(w, ArtworkNotFound)
		return
	}
	if artwork.PostID != uuid.Nil {
		app.badRequestErrorResponse(w, ArtworkAlreadyHasPost)
		return
	}

	if err = app.validateTags(r, req.Data.TagIds); err != nil {
		app.notFoundErrorResponse(w, TagNotFound)
		return
	}

	post := db.CreatePostParams{
		ID:    uuid.New(),
		Title: req.Data.Title,
		Description: pgtype.Text{
			String: req.Data.Description,
			Valid:  true,
		},
		UserID: user.ID,
	}

	updatedArtwork := db.UpdateArtworkParams{
		ID:        artworkId,
		IsBuyable: req.Data.IsBuyable,
		PostID:    post.ID,
	}

	artworkPrice := db.CreateArtworkPriceParams{
		ID: uuid.New(),
		Price: pgtype.Int8{
			Int64: int64(req.Data.Price),
			Valid: true,
		},
		ArtworkID: artworkId,
	}

	err = app.createPostTransaction(r.Context(), post, updatedArtwork, req.Data.TagIds, artworkPrice)
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          map[string]uuid.UUID{"postId": post.ID},
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) updatePostHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	var req request.UpdatePostRequest
	err = app.readJson(w, r, &req)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	v := validator.New()
	if request.ValidateUpdatePostRequest(v, req); !v.Valid() {
		app.failedValidationResponse(w, v.Errors)
		return
	}

	postId, err := app.readIDParam(r, "postId")
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	post, err := app.queries.GetPostById(r.Context(), postId)
	if err != nil {
		app.notFoundErrorResponse(w, PostNotFound)
		return
	}

	user, err := app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	if post.UserID != user.ID {
		app.unauthorizedErrorResponse(w, UnauthorizedToUpdatePost)
		return
	}

	if err = app.validateTags(r, req.Data.TagIds); err != nil {
		app.notFoundErrorResponse(w, err.Error())
		return
	}

	if req.Data.IsBuyable && req.Data.Price <= 0 {
		app.badRequestErrorResponse(w, PriceSmallerThanZero)
		return
	}

	if err = app.updatePostTransaction(r.Context(), req, post); err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          nil,
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) deletePostHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	postId, err := app.readIDParam(r, "postId")
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	post, err := app.queries.GetPostById(r.Context(), postId)
	if err != nil {
		app.notFoundErrorResponse(w, PostNotFound)
		return
	}

	user, err := app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, enum.UnauthorizedMessage)
		return
	}
	if post.UserID != user.ID {
		app.unauthorizedErrorResponse(w, UnauthorizedToDeletePost)
		return
	}

	deletedPost := db.DeletePostByIdParams{
		ID:     postId,
		UserID: user.ID,
	}
	if err = app.deletePostTransaction(r.Context(), deletedPost); err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          nil,
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) increasePostViewsHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	postId, err := app.readIDParam(r, "postId")
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
	}

	if isPostExisted, err := app.queries.IsPostExistedById(r.Context(), postId); err != nil || !isPostExisted {
		app.notFoundErrorResponse(w, PostNotFound)
		return
	}

	err = app.queries.AddViewToPost(r.Context(), postId)
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          nil,
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) getPostInfoByIdHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	postId, err := app.readIDParam(r, "postId")
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	userId, err := app.extractUserIdIfHasToken(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	post, err := app.queries.GetPostInfoById(r.Context(), db.GetPostInfoByIdParams{
		ID:     postId,
		UserID: userId,
	})
	if err != nil {
		app.notFoundErrorResponse(w, err.Error())
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          post,
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) searchPostsHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	pageNumber, err := strconv.Atoi(app.readStrings(r.URL.Query(), "page", "1"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	pageSize, err := strconv.Atoi(app.readStrings(r.URL.Query(), "size", "10"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	userId, err := app.extractUserIdIfHasToken(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	searchTerm := app.readStrings(r.URL.Query(), "searchTerm", "")

	sortBy := app.readStrings(r.URL.Query(), "sortBy", "title")
	sortOrder := app.readStrings(r.URL.Query(), "sortOrder", "DESC")
	isBuyableStr := app.readStrings(r.URL.Query(), "isBuyable", "false")
	isBuyable, err := strconv.ParseBool(isBuyableStr)
	if err != nil {
		isBuyable = false
	}
	tagIds := app.readStrings(r.URL.Query(), "tagIds", "")
	var tagIdsArr []uuid.UUID
	if tagIds != "" && tagIds != enum.Undefined {
		tagIdsArr, err = parseTagsId(tagIds)
		if err != nil {
			app.badRequestErrorResponse(w, err.Error())
			return
		}
	}
	posts, err := app.queries.GetPostsByUserAndTags(r.Context(), db.GetPostsByUserAndTagsParams{
		Size:       int32(pageSize),
		Page:       int32(pageNumber),
		SortBy:     sortBy,
		SortOrder:  sortOrder,
		SearchTerm: searchTerm,
		IsBuyable:  isBuyable,
		TagIds:     tagIdsArr,
		UserID:     userId,
	})

	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          posts,
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) getPostsByArtistHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	artistId, err := app.readIDParam(r, "userId")
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	userId, err := app.extractUserIdIfHasToken(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	pageNumber, err := strconv.Atoi(app.readStrings(r.URL.Query(), "page", "1"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	pageSize, err := strconv.Atoi(app.readStrings(r.URL.Query(), "size", "10"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	sortBy := app.readStrings(r.URL.Query(), "sortBy", "created_at")
	sortOrder := app.readStrings(r.URL.Query(), "sortOrder", "DESC")

	posts, err := app.queries.GetPostsByArtist(r.Context(), db.GetPostsByArtistParams{
		Size:      int32(pageSize),
		Page:      int32(pageNumber),
		SortBy:    sortBy,
		SortOrder: sortOrder,
		ArtistID:  artistId,
		UserID:    userId,
	})

	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          posts,
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) getPostsByCollectionHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	userId, err := app.extractUserIdIfHasToken(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	collectionId, err := app.readIDParam(r, "collectionId")
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	if isCollectionExisted, err := app.queries.IsCollectionExistedById(r.Context(), collectionId); err != nil || !isCollectionExisted {
		app.notFoundErrorResponse(w, CollectionNotFound)
		return
	}

	pageNumber, err := strconv.Atoi(app.readStrings(r.URL.Query(), "page", "1"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	pageSize, err := strconv.Atoi(app.readStrings(r.URL.Query(), "size", "4"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	posts, err := app.queries.GetPostsByCollection(r.Context(), db.GetPostsByCollectionParams{
		Size:         int32(pageSize),
		Page:         int32(pageNumber),
		UserID:       userId,
		CollectionID: collectionId,
	})

	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          posts,
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) createPostTransaction(ctx context.Context, post db.CreatePostParams,
	updatedArtwork db.UpdateArtworkParams, tagIds []uuid.UUID,
	artworkPrice db.CreateArtworkPriceParams) error {

	tx, err := app.dbPool.Begin(ctx)
	if err != nil {
		return fmt.Errorf(FailureToBeginDBTransaction)
	}
	defer func(tx pgx.Tx, ctx context.Context) {
		_ = tx.Rollback(ctx)
	}(tx, ctx)

	qtx := app.queries.WithTx(tx)

	if err = qtx.CreatePost(ctx, post); err != nil {
		return fmt.Errorf(FailureToCreatePost, err)
	}

	if err = qtx.UpdateArtwork(ctx, updatedArtwork); err != nil {
		return fmt.Errorf(FailureToUpdateArtwork, err)
	}

	for _, tagId := range tagIds {
		err = qtx.CreatePostTags(ctx, db.CreatePostTagsParams{
			PostID: post.ID,
			TagID:  tagId,
		})
		if err != nil {
			return fmt.Errorf(FailureToCreatePostTags, err)
		}
	}

	if err = qtx.CreateArtworkPrice(ctx, artworkPrice); err != nil {
		return fmt.Errorf(FailureToCreatePrice, err)
	}

	err = tx.Commit(ctx)
	if err != nil {
		return fmt.Errorf(FailureToCommitDBTransaction)
	}
	return nil
}

func (app *application) updatePostTransaction(ctx context.Context, request request.UpdatePostRequest,
	post db.GetPostByIdRow) error {

	tx, err := app.dbPool.Begin(ctx)
	if err != nil {
		return fmt.Errorf(FailureToBeginDBTransaction)
	}
	defer func(tx pgx.Tx, ctx context.Context) {
		_ = tx.Rollback(ctx)
	}(tx, ctx)

	qtx := app.queries.WithTx(tx)

	updatedPost := db.UpdatePostByIdParams{
		ID:    post.ID,
		Title: defaultIfEmptyString(request.Data.Title, post.Title),
		Description: pgtype.Text{
			String: defaultIfEmptyString(request.Data.Description, post.Description.String),
			Valid:  true,
		},
		UserID: post.UserID,
	}

	if err = qtx.UpdatePostById(ctx, updatedPost); err != nil {
		return fmt.Errorf(FailureToUpdatePost, err)
	}

	if err = qtx.DeletePostTags(ctx, updatedPost.ID); err != nil {
		return fmt.Errorf(FailureToDeletePostTags, err)
	}

	for _, tagId := range request.Data.TagIds {
		err = qtx.CreatePostTags(ctx, db.CreatePostTagsParams{
			PostID: updatedPost.ID,
			TagID:  tagId,
		})
		if err != nil {
			return fmt.Errorf(FailureToCreatePostTags, err)
		}
	}

	newPrice := pgtype.Int8{
		Int64: int64(request.Data.Price),
		Valid: true,
	}

	if post.IsBuyable == request.Data.IsBuyable && post.Price.Int64 == newPrice.Int64 {
		err = app.commitDBTransaction(ctx, tx)
		if err != nil {
			return err
		}
		return nil
	}

	if !request.Data.IsBuyable {
		newPrice.Int64 = 0
	}

	updatedArtwork := db.UpdateArtworkParams{
		ID:        post.ArtworkID,
		IsBuyable: request.Data.IsBuyable,
		PostID:    post.ID,
	}

	if err = qtx.UpdateArtwork(ctx, updatedArtwork); err != nil {
		return fmt.Errorf(FailureToUpdateArtwork, err)
	}

	err = qtx.UpdateArtworkPrice(ctx, updatedArtwork.ID)
	if err != nil {
		return fmt.Errorf(FailureToUpdatePrice, err)
	}

	err = qtx.CreateArtworkPrice(ctx, db.CreateArtworkPriceParams{
		ID:        uuid.New(),
		Price:     newPrice,
		ArtworkID: post.ArtworkID,
	})
	if err != nil {
		return fmt.Errorf(FailureToCreatePrice, err)
	}

	if err = app.commitDBTransaction(ctx, tx); err != nil {
		return err
	}
	return nil
}

func (app *application) deletePostTransaction(ctx context.Context, deletedPost db.DeletePostByIdParams) error {

	tx, err := app.dbPool.Begin(ctx)
	if err != nil {
		return fmt.Errorf(FailureToBeginDBTransaction)
	}
	defer func(tx pgx.Tx, ctx context.Context) {
		_ = tx.Rollback(ctx)
	}(tx, ctx)

	qtx := app.queries.WithTx(tx)

	if err = qtx.DeletePostById(ctx, deletedPost); err != nil {
		return fmt.Errorf(FailureToDeletePost, err)
	}

	if err = qtx.DeletePostTags(ctx, deletedPost.ID); err != nil {
		return fmt.Errorf(FailureToDeletePostTags, err)
	}

	artwork, err := qtx.GetArtworkByPost(ctx, deletedPost.ID)
	if err != nil {
		return fmt.Errorf(ArtworkNotFound)
	}

	if err = app.deleteImage(artwork.ImagePath, artwork.ProcessedImagePath); err != nil {
		return fmt.Errorf(enum.FailureToDeleteFile, err)
	}

	if err = qtx.DeleteArtworkSoft(ctx, artwork.ID); err != nil {
		return fmt.Errorf(FailureToDeleteArtwork)
	}

	if err = app.queries.UpdateArtworkPrice(ctx, artwork.ID); err != nil {
		return fmt.Errorf(FailureToUpdatePrice, err)
	}

	err = app.commitDBTransaction(ctx, tx)
	if err != nil {
		return err
	}

	return nil
}

func parseTagsId(tagIds string) ([]uuid.UUID, error) {
	tagIdsStr := strings.Split(tagIds, ", ")

	var result []uuid.UUID

	for _, idStr := range tagIdsStr {
		id, err := uuid.Parse(idStr)
		if err != nil {
			return nil, err
		}

		result = append(result, id)
	}

	return result, nil
}

func (app *application) validateTags(r *http.Request, tagIds []uuid.UUID) error {
	tags, err := app.queries.CountTagsByIds(r.Context(), tagIds)
	if err != nil || int(tags) != len(tagIds) {
		return fmt.Errorf(TagNotFound)
	}
	return nil
}
