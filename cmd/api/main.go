package main

import (
	"be-omnistroke/db/sqlc"
	"be-omnistroke/internal/pkg/enum"
	"be-omnistroke/internal/pkg/jsonlog"
	"context"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/joho/godotenv"
	"os"
	"strconv"
	"sync"
)

type config struct {
	port             int
	domain           string
	imageDomain      string
	feePercentage    float64
	secretKey        string
	defaultBanner    string
	defaultAvatar    string
	payosApiKey      string
	payosClientId    string
	payosChecksumKey string
	returnServiceURL string
	returnAppURL     string
	db               struct {
		dsn          string
		maxOpenConns int
		maxIdleConns int
		maxIdleTime  string
	}

	cors struct {
		trustedOrigins []string
	}
}

type application struct {
	config  config
	queries *db.Queries
	dbPool  *pgxpool.Pool
	logger  *jsonlog.Logger
	wg      sync.WaitGroup
}

func main() {
	// Configure env and logger
	err := godotenv.Load(enum.EnvFile)
	logr := jsonlog.NewLogger(os.Stdout, jsonlog.LevelInfo)
	if err != nil {
		logr.PrintFatal(err, nil)
	}

	var cfg config
	// Load configurations
	port, err := strconv.Atoi(os.Getenv(enum.AppPort))
	if err != nil {
		logr.PrintFatal(err, nil)
	}
	cfg.port = port
	cfg.domain = os.Getenv(enum.AppDomain)
	cfg.imageDomain = os.Getenv(enum.AppImageDomain)
	cfg.defaultBanner = os.Getenv(enum.AppDefaultBanner)
	cfg.defaultAvatar = os.Getenv(enum.AppDefaultAvatar)
	cfg.db.dsn = os.Getenv(enum.DbDsn)
	//payos
	cfg.payosApiKey = os.Getenv(enum.PayosApiKey)
	cfg.payosClientId = os.Getenv(enum.PayosClientId)
	cfg.payosChecksumKey = os.Getenv(enum.PayosChecksumKey)

	// Return URL
	cfg.returnServiceURL = os.Getenv(enum.ReturnServiceURL)
	cfg.returnAppURL = os.Getenv(enum.ReturnAppURL)

	cfg.feePercentage, err = strconv.ParseFloat(os.Getenv(enum.FeePercentage), 64)
	if err != nil {
		logr.PrintFatal(err, nil)
	}
	cfg.secretKey = os.Getenv(enum.SecretKey)

	// Create connection pool
	pool, err := pgxpool.New(context.Background(), cfg.db.dsn)

	if err != nil {
		logr.PrintFatal(err, nil)
	}
	defer pool.Close()

	// Start application
	app := &application{
		config:  cfg,
		logger:  logr,
		dbPool:  pool,
		queries: db.New(pool),
	}

	if err := app.serve(); err != nil {
		logr.PrintFatal(err, nil)
	}
}
