package main

import (
	db "be-omnistroke/db/sqlc"
	"be-omnistroke/internal/pkg/dto/payload"
	"be-omnistroke/internal/pkg/enum"
	"net/http"
)

func (app *application) likePostHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	postId, err := app.readIDParam(r, "postId")
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}
	if isPostExisted, err := app.queries.IsPostExistedById(r.Context(), postId); err != nil || !isPostExisted {
		app.notFoundErrorResponse(w, PostNotFound)
		return
	}

	user, err := app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	likeStatus, err := app.queries.CheckLikeStatus(r.Context(), db.CheckLikeStatusParams{
		UserID: user.ID,
		PostID: postId,
	})
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}
	if !likeStatus {
		err = app.queries.LikePost(r.Context(), db.LikePostParams{
			UserID: user.ID,
			PostID: postId,
		})
	} else {
		err = app.queries.UnlikePost(r.Context(), db.UnlikePostParams{
			UserID: user.ID,
			PostID: postId,
		})

	}
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          nil,
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}
