package main

import (
	db "be-omnistroke/db/sqlc"
	"be-omnistroke/internal/pkg/dto/payload"
	"be-omnistroke/internal/pkg/dto/payload/request"
	"be-omnistroke/internal/pkg/enum"
	"be-omnistroke/internal/pkg/validator"
	"context"
	"fmt"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
	"net/http"
	"strconv"
	"strings"
)

const (
	TransactionTypeBuy      = 1
	TransactionTypeReceive  = 2
	TransactionTypeWithdraw = 3

	TransactionStatusPending = 1
	TransactionStatusSuccess = 2
	TransactionStatusFailure = 3

	FailureToCreateTransaction = "failed to create transaction: %w"
	FailureToUpdateTransaction = "failed to update transaction: %w"
	InSufficientBalance        = "insufficient balance!"
	TransactionNotFound        = "transaction not found!"
	TransactionNotPending      = "transaction is not pending"

	InvalidKeyword = "invalid keyword"
	InvalidValue   = "invalid value"
)

func (app *application) withdrawMoneyHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	var req request.WithdrawTransactionRequest
	err = app.readJson(w, r, &req)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	v := validator.New()
	if request.ValidateWithdrawTransactionRequest(v, req); !v.Valid() {
		app.failedValidationResponse(w, v.Errors)
		return
	}

	user, err := app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	balance, err := app.queries.GetWallet(r.Context(), user.ID)
	if err != nil {
		app.notFoundErrorResponse(w, WalletNotFound)
		return
	}

	if balance < int64(req.Data.Amount) {
		app.badRequestErrorResponse(w, InSufficientBalance)
		return
	}

	transaction := db.WithdrawMoneyParams{
		ID:     uuid.New(),
		Type:   TransactionTypeWithdraw,
		Status: TransactionStatusPending,
		Amount: int64(req.Data.Amount),
		UserID: user.ID,
	}

	userWallet := db.UpdateWalletBalanceParams{
		UserID: user.ID,
		Amount: -int64(req.Data.Amount),
	}

	err = app.withdrawMoneyTransaction(r.Context(), transaction, userWallet)
	if err != nil {
		app.serverErrorResponse(w, err)
	}

	if err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data: map[string]uuid.UUID{
			"transactionId": transaction.ID,
		},
	}, nil); err != nil {
		app.serverErrorResponse(w, err)

	}
}

func (app *application) updateWithdrawMoneyStatusHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	var req request.UpdateWithdrawMoneyStatusRequest
	err = app.readJson(w, r, &req)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	v := validator.New()
	if request.ValidateUpdateWithdrawMoneyStatusRequest(v, req); !v.Valid() {
		app.failedValidationResponse(w, v.Errors)
		return
	}

	userId, _ := uuid.Parse(req.Data.UserId)

	moderator, err := app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	transactionId, err := app.readIDParam(r, "transactionId")
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	transaction, err := app.queries.GetTransactionByIdAndUser(r.Context(), db.GetTransactionByIdAndUserParams{
		ID:     transactionId,
		UserID: userId,
	})
	if err != nil {
		app.notFoundErrorResponse(w, TransactionNotFound)
		return
	}
	if transaction.Status != TransactionStatusPending {
		app.badRequestErrorResponse(w, TransactionNotPending)
		return
	}
	var amount int64
	amount = 0
	if getTransactionStatus(req.Data.Status) == TransactionStatusFailure {
		amount = transaction.Amount
	}

	updateTransaction := db.UpdateWithdrawMoneyStatusParams{
		ID:          transaction.ID,
		ModeratorID: moderator.ID,
		UserID:      userId,
		Status:      getTransactionStatus(req.Data.Status),
	}

	balance := db.UpdateWalletBalanceParams{
		UserID: userId,
		Amount: amount,
	}

	err = app.updateWithdrawMoneyStatusTransaction(r.Context(), updateTransaction, balance)
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	if err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          nil,
	}, nil); err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) getTransactionByIdHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	transactionId, err := app.readIDParam(r, "transactionId")
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	_, err = app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	//getTransactionParam := db.GetTransactionByIdAndUserParams{
	//	ID:     transactionId,
	//	UserID: user.ID,
	//}

	transaction, err := app.queries.GetTransactionById(r.Context(), transactionId)
	if err != nil {
		app.notFoundErrorResponse(w, TransactionNotFound)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          transaction,
	}, nil)

	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) getTransactionHistoryHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	user, err := app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}
	pageNumber, err := strconv.Atoi(app.readStrings(r.URL.Query(), "page", "1"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	pageSize, err := strconv.Atoi(app.readStrings(r.URL.Query(), "size", "10"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	keyword := app.readStrings(r.URL.Query(), "keyword", "type")
	if !strings.EqualFold(keyword, "type") && !strings.EqualFold(keyword, "status") {
		app.badRequestErrorResponse(w, InvalidKeyword)
	}

	value, err := strconv.Atoi(app.readStrings(r.URL.Query(), "value", "0"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}
	if value < 0 || value > 3 {
		app.badRequestErrorResponse(w, InvalidValue)
		return
	}

	sortBy := app.readStrings(r.URL.Query(), "sortBy", "transactionDate")
	sortOrder := app.readStrings(r.URL.Query(), "sortOrder", "DESC")

	getHistoryParams := db.GetTransactionsHistoryParams{
		Size:      int32(pageSize),
		Page:      int32(pageNumber),
		SortBy:    sortBy,
		SortOrder: sortOrder,
		UserID:    user.ID,
		Keyword:   keyword,
		Value:     int16(value),
	}

	transactions, err := app.queries.GetTransactionsHistory(r.Context(), getHistoryParams)
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          transactions,
	}, nil)

	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) getTransactionsPaginatedHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	_, err = app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	keyword := app.readStrings(r.URL.Query(), "keyword", "type")
	if !strings.EqualFold(keyword, "type") && !strings.EqualFold(keyword, "status") {
		app.badRequestErrorResponse(w, InvalidKeyword)
	}

	value, err := strconv.Atoi(app.readStrings(r.URL.Query(), "value", "1"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}
	if value < 1 || value > 3 {
		app.badRequestErrorResponse(w, InvalidValue)
		return
	}

	pageNumber, err := strconv.Atoi(app.readStrings(r.URL.Query(), "page", "1"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	pageSize, err := strconv.Atoi(app.readStrings(r.URL.Query(), "size", "10"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	sortBy := app.readStrings(r.URL.Query(), "sortBy", "transactionDate")
	sortOrder := app.readStrings(r.URL.Query(), "sortOrder", "DESC")

	transactions, err := app.queries.GetTransactionsPaginated(r.Context(), db.GetTransactionsPaginatedParams{
		Size:      int32(pageSize),
		Page:      int32(pageNumber),
		SortBy:    sortBy,
		SortOrder: sortOrder,
		Keyword:   keyword,
		Value:     int16(value),
	})

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          transactions,
	}, nil)

	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) withdrawMoneyTransaction(ctx context.Context, transaction db.WithdrawMoneyParams,
	userWallet db.UpdateWalletBalanceParams) error {

	tx, err := app.dbPool.Begin(ctx)
	if err != nil {
		return fmt.Errorf(FailureToBeginDBTransaction)
	}
	defer func(tx pgx.Tx, ctx context.Context) {
		_ = tx.Rollback(ctx)
	}(tx, ctx)

	qtx := app.queries.WithTx(tx)

	err = qtx.WithdrawMoney(ctx, transaction)
	if err != nil {
		return fmt.Errorf(FailureToCreateTransaction, err)
	}

	err = qtx.UpdateWalletBalance(ctx, userWallet)
	if err != nil {
		return fmt.Errorf(FailureToUpdateBalance, err)
	}

	if err = app.commitDBTransaction(ctx, tx); err != nil {
		return err
	}

	return nil
}

func (app *application) updateWithdrawMoneyStatusTransaction(ctx context.Context, transaction db.UpdateWithdrawMoneyStatusParams,
	updateWallet db.UpdateWalletBalanceParams) error {

	tx, err := app.dbPool.Begin(ctx)
	if err != nil {
		return fmt.Errorf(FailureToBeginDBTransaction)
	}
	defer func(tx pgx.Tx, ctx context.Context) {
		_ = tx.Rollback(ctx)
	}(tx, ctx)

	qtx := app.queries.WithTx(tx)
	if err = qtx.UpdateWithdrawMoneyStatus(ctx, transaction); err != nil {
		return fmt.Errorf(FailureToUpdateTransaction, err)
	}

	if err = qtx.UpdateWalletBalance(ctx, updateWallet); err != nil {
		return fmt.Errorf(FailureToUpdateBalance, err)
	}

	if err = app.commitDBTransaction(ctx, tx); err != nil {
		return err
	}

	return nil
}

func getTransactionStatus(input int) int16 {
	switch input {
	case 0:
		return TransactionStatusPending
	case 1:
		return TransactionStatusFailure
	case 2:
		return TransactionStatusSuccess
	default:
		return TransactionStatusPending
	}
}
