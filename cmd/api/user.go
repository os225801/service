package main

import (
	db "be-omnistroke/db/sqlc"
	"be-omnistroke/internal/pkg/dto/payload"
	"be-omnistroke/internal/pkg/dto/payload/request"
	"be-omnistroke/internal/pkg/enum"
	"be-omnistroke/internal/pkg/validator"
	"fmt"
	"github.com/google/uuid"
	"github.com/h2non/bimg"
	"github.com/jackc/pgx/v5/pgtype"
	"net/http"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

const (
	UserNotFound       = "user not found!"
	UserIdErrorMessage = "User id is not valid"

	ErrorParsingDOB    = "error parsing date of birth: %v"
	DateFormat         = "02/01/2006"
	ErrorDOBBefore1900 = "date of birth cannot be before 1900"
	ErrorDOBInFuture   = "date of birth cannot be in the future"
	RequiredDOB        = "date of birth is required"

	FailureToCreateUser = "failed to create user: %w"

	AvatarFile = "avatar"
	BannerFile = "banner"
)

func (app *application) updateUserInfoHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	user, err := app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	var req request.UpdateUserInfoRequest
	err = app.readJson(w, r, &req)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	v := validator.New()
	if request.ValidateUpdateUserInfoRequest(v, req); !v.Valid() {
		app.failedValidationResponse(w, v.Errors)
		return
	}

	if !user.DateOfBirth.Valid && req.Data.DateOfBirth == "" {
		app.badRequestErrorResponse(w, RequiredDOB)
		return
	}
	user.DateOfBirth, err = parseDateOfBirth(defaultIfEmptyString(req.Data.DateOfBirth, user.DateOfBirth.Time.Format(DateFormat)))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	err = app.queries.UpdateUserInfo(r.Context(), db.UpdateUserInfoParams{
		Name:          defaultIfEmptyString(req.Data.Name, user.Name),
		DateOfBirth:   user.DateOfBirth,
		AccountNumber: defaultIfEmptyString(req.Data.AccountNumber, user.AccountNumber.String),
		BankName:      defaultIfEmptyString(req.Data.BankName, user.BankName.String),
		AccountName:   defaultIfEmptyString(req.Data.AccountName, user.AccountName.String),
		ID:            user.ID,
	})
	if err != nil {
		app.serverErrorResponse(w, err)
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) updateUserImageHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	user, err := app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	user.AvatarPath, err = app.processUserImage(r, AvatarFile, user.AvatarPath.String, user.ID)
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	user.BannerPath, err = app.processUserImage(r, BannerFile, user.BannerPath.String, user.ID)
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.queries.UpdateUserImage(r.Context(), db.UpdateUserImageParams{
		AvatarPath: user.AvatarPath,
		BannerPath: user.BannerPath,
		AvatarLink: app.createImageLink(user.AvatarPath.String),
		BannerLink: app.createImageLink(user.BannerPath.String),
		ID:         user.ID,
	})
	if err != nil {
		app.serverErrorResponse(w, err)
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) getUserProfileHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	userId, err := app.readIDParam(r, "userId")
	if err != nil {
		app.badRequestErrorResponse(w, UserIdErrorMessage)
		return
	}

	guestId, err := app.extractUserIdIfHasToken(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	user, err := app.queries.GetProfileInfo(r.Context(), db.GetProfileInfoParams{
		FollowerID: guestId,
		UserID:     userId,
	})
	if err != nil {
		app.notFoundErrorResponse(w, UserNotFound)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          user,
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) getCurrentUserHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	user, err := app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	userInfo, _ := app.queries.GetCurrentUserProfile(r.Context(), user.ID)

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          userInfo,
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) countAllUsersHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	count, err := app.queries.CountAllUsers(r.Context())
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          map[string]int64{"totalUsers": count},
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) getAllLoggedInUsersHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	pageNumber, err := strconv.Atoi(app.readStrings(r.URL.Query(), "page", "1"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	pageSize, err := strconv.Atoi(app.readStrings(r.URL.Query(), "size", "10"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	searchTerm := app.readStrings(r.URL.Query(), "searchTerm", "")
	sortBy := app.readStrings(r.URL.Query(), "sortBy", "expiryDate")
	sortOrder := app.readStrings(r.URL.Query(), "sortOrder", "DESC")

	users, err := app.queries.GetLoggedInUser(r.Context(), db.GetLoggedInUserParams{
		Size:       int32(pageSize),
		Page:       int32(pageNumber),
		SortBy:     sortBy,
		SortOrder:  sortOrder,
		SearchTerm: searchTerm,
	})

	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          users,
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}

}

func (app *application) processUserImage(r *http.Request, formFieldName string, currentPath string, userId uuid.UUID) (pgtype.Text, error) {
	file, err := app.readFormFile(r, formFieldName)
	if err != nil {
		if err.Error() != enum.FailureToGetFile {
			return pgtype.Text{}, err
		}
		return pgtype.Text{
			String: currentPath,
			Valid:  true,
		}, nil
	}

	fileBuffer, err := app.readFileBuffer(file)
	if err != nil {
		return pgtype.Text{}, err
	}

	if strings.Contains(currentPath, userId.String()) {
		err = app.deleteImage(currentPath, formFieldName)
		if err != nil {
			return pgtype.Text{
				String: "",
				Valid:  false,
			}, err
		}
	}

	filePath, err := saveUserImage(fileBuffer, formFieldName, userId)
	if err != nil {
		return pgtype.Text{
			String: "",
			Valid:  false,
		}, err
	}

	return pgtype.Text{
		String: defaultIfEmptyString(filePath, currentPath),
		Valid:  true,
	}, nil
}

func saveUserImage(buffer []byte, fileName string, userId uuid.UUID) (string, error) {
	userFolder, err := getUserDirectory(userId)
	if err != nil {
		return "", err
	}

	convertImage, err := bimg.NewImage(buffer).Convert(bimg.WEBP)
	if err != nil {
		return "", fmt.Errorf(enum.FailureToConvertImage, err)
	}

	imageName := fmt.Sprintf(enum.WebpFile, fileName)
	filePath := filepath.Join(userFolder, imageName)

	err = bimg.Write(filePath, convertImage)
	if err != nil {
		return "", fmt.Errorf(enum.FailureToSaveImage, err)
	}

	return filePath, nil
}

func parseDateOfBirth(dateOfBirth string) (pgtype.Timestamp, error) {
	dob, err := time.Parse(DateFormat, dateOfBirth)
	if err != nil {
		return pgtype.Timestamp{Valid: false}, fmt.Errorf(ErrorParsingDOB, err)
	}
	if dob.After(time.Now()) {
		return pgtype.Timestamp{Valid: false}, fmt.Errorf(ErrorDOBInFuture)
	}
	if dob.Before(time.Date(1900, 1, 1, 0, 0, 0, 0, time.UTC)) {
		return pgtype.Timestamp{Valid: false}, fmt.Errorf(ErrorDOBBefore1900)
	}
	return pgtype.Timestamp{
		Time:  dob,
		Valid: true,
	}, nil
}
