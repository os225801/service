package main

import (
	"be-omnistroke/internal/pkg/enum"
	"be-omnistroke/internal/pkg/token"
	"fmt"
	"net/http"
	"strings"
)

func (app *application) recoverPanic(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				writer.Header().Set("Connection", "close")
				app.serverErrorResponse(writer, fmt.Errorf("%s", err))
			}
		}()

		next.ServeHTTP(writer, request)
	})
}

func (app *application) cors(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		writer.Header().Set("Vary", "Origin")
		writer.Header().Set("Vary", "Access-Control-Request-Method")

		origin := request.Header.Get("Origin")
		if origin != "" {
			for i := range app.config.cors.trustedOrigins {
				if origin == app.config.cors.trustedOrigins[i] {
					writer.Header().Set("Access-Control-Allow-Origin", origin)
					if request.Method == http.MethodOptions && request.Header.Get("Access-Control-Request-Method") != "" {
						writer.Header().Set("Access-Control-Allow-Methods", "OPTIONS, PUT, PATCH, DELETE, POST, GET")
						writer.Header().Set("Access-Control-Allow-Headers", "Authorization, Content-Type")
						writer.Header().Set("Access-Control-Max-Age", "60")
						writer.WriteHeader(http.StatusOK)
						return
					}
					break
				}
			}
		}

		next.ServeHTTP(writer, request)
	})
}

func (app *application) authenticate(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		writer.Header().Set(enum.Vary, enum.HttpAuthorization)
		authorizationHeader := request.Header.Get(enum.HttpAuthorization)

		parts := strings.Split(authorizationHeader, " ")
		if len(parts) != 2 || parts[0] != enum.Bearer {
			next.ServeHTTP(writer, request)
			return
		}

		tkn := parts[1]

		_, err := token.GetClaims(tkn, app.config.secretKey)
		if err != nil {
			app.badRequestErrorResponse(writer, err.Error())
			return
		}

		request = app.contextSetToken(request, tkn)
		next.ServeHTTP(writer, request)
	})
}

func (app *application) requireAuthentication(next http.HandlerFunc) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		_, err := app.contextGetToken(request)

		if err != nil {
			app.unauthorizedErrorResponse(writer, err.Error())
			return
		}

		next.ServeHTTP(writer, request)
	}
}
