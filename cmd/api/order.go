package main

import (
	db "be-omnistroke/db/sqlc"
	"be-omnistroke/internal/pkg/dto/payload"
	"be-omnistroke/internal/pkg/dto/payload/request"
	"be-omnistroke/internal/pkg/enum"
	"be-omnistroke/internal/pkg/validator"
	"context"
	"fmt"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgtype"
	"github.com/payOSHQ/payos-lib-golang"
	"math"
	"net/http"
	"strconv"
	"time"
)

const (
	OrderStatusPending = 1
	OrderStatusSuccess = 2
	OrderStatusFailure = 3

	OrderNotFound   = "order not found!"
	OrderNotPending = "order is not pending"

	CannotAccessResource = "can not access this resource"
	CannotBuyOwnArtwork  = "an artist cannot buy their own artwork"
	UserAlreadyBought    = "user already bought this artwork"

	FailureToUpdateBalance     = "failed to update balance: %w"
	FailureToUpdateOrderStatus = "failed to update order status: %w"
	FailureToApproveOrder      = "failed to approve order: %w"
	FailureToGetSellerWallet   = "failed to get seller wallet: %w"
	FailureToCreateOrder       = "failed to create order: %w"
	FailureToCreatePaymentLink = "failed to create payment link: %w"
	FailureToConnectPayOS      = "failed to connect to PayOS: %w"

	PaymentLinkExpired   = "EXPIRED"
	PaymentLinkCancelled = "CANCELLED"

	TransferContent = "Thanh toán đơn hàng "
	ExpiredTime     = 15

	CancelURL   = "/orders/cancel"
	SuccessURL  = "/orders/success"
	Order       = "/orders/"
	Transaction = "/transactions/"
)

func (app *application) createOrderHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	var req request.CreateOrderRequest
	err = app.readJson(w, r, &req)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	v := validator.New()
	if request.ValidateCreateOrderRequest(v, req); !v.Valid() {
		app.failedValidationResponse(w, v.Errors)
		return
	}

	user, err := app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	artworkId, _ := uuid.Parse(req.Data.ArtworkId)
	artwork, err := app.queries.GetArtworkById(r.Context(), artworkId)
	if err != nil {
		app.notFoundErrorResponse(w, ArtworkNotFound)
		return
	}
	if artwork.PostID == uuid.Nil || artwork.IsDeleted {
		app.notFoundErrorResponse(w, ArtworkNotFound)
		return
	}
	if artwork.ArtistID == user.ID {
		app.badRequestErrorResponse(w, CannotBuyOwnArtwork)
		return
	}
	if !artwork.IsBuyable {
		app.badRequestErrorResponse(w, ArtworkNotForSale)
		return
	}

	postName, err := app.queries.GetPostTitleById(r.Context(), artwork.PostID)
	if err != nil {
		app.notFoundErrorResponse(w, PostNotFound)
		return
	}

	orderExisted, _ := app.queries.GetOrderByArtworkAndBuyer(r.Context(), db.GetOrderByArtworkAndBuyerParams{
		ArtworkID: artworkId,
		BuyerID:   user.ID,
	})

	if orderExisted.Status == OrderStatusSuccess {
		app.badRequestErrorResponse(w, UserAlreadyBought)
		return
	}

	if orderExisted.Status == OrderStatusPending {
		if err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
			ResultCode:    enum.SuccessCode,
			ResultMessage: enum.SuccessMessage,
			Data: map[string]interface{}{
				"paymentLink": orderExisted.PaymentLink.String,
			},
		}, nil); err != nil {
			app.serverErrorResponse(w, err)
			return
		}

		return
	}

	price, err := app.queries.GetArtworkPriceByArtwork(r.Context(), artworkId)
	if err != nil {
		app.notFoundErrorResponse(w, PriceNotFound)
		return
	}

	order := db.CreateOrderParams{
		ID:        uuid.New(),
		Amount:    price,
		Status:    OrderStatusPending,
		BuyerID:   user.ID,
		SellerID:  artwork.ArtistID,
		ArtworkID: artworkId,
	}

	paymentLink, err := app.createOrderTransaction(r.Context(), order, user, postName)
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	if err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data: map[string]interface{}{
			"paymentLink": paymentLink,
		},
	}, nil); err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) cancelOrderHandler(w http.ResponseWriter, r *http.Request) {
	user, err := app.getCurrentUser(r)
	if err == nil || user != nil {
		app.unauthorizedErrorResponse(w, CannotAccessResource)
	}

	orderCode, err := strconv.Atoi(app.readStrings(r.URL.Query(), "orderCode", ""))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	order, err := app.queries.GetOrderByCode(r.Context(), int32(orderCode))
	if err != nil {
		app.notFoundErrorResponse(w, OrderNotFound)
		return
	}

	err = app.queries.UpdateOrderStatus(r.Context(), db.UpdateOrderStatusParams{
		Status:    OrderStatusFailure,
		OrderCode: int32(orderCode),
	})
	if err != nil {
		app.serverErrorResponse(w, err)
	}

	http.Redirect(w, r, app.config.returnAppURL+Order+order.ID.String(), http.StatusSeeOther)
}

func (app *application) updateOrderStatusHandler(w http.ResponseWriter, r *http.Request) {
	user, err := app.getCurrentUser(r)
	if err == nil || user != nil {
		app.unauthorizedErrorResponse(w, CannotAccessResource)
	}

	orderCode, err := strconv.Atoi(app.readStrings(r.URL.Query(), "orderCode", ""))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	order, err := app.queries.GetOrderByCode(r.Context(), int32(orderCode))
	if err != nil {
		app.notFoundErrorResponse(w, OrderNotFound)
		return
	}

	orderUpdate := db.UpdateOrderStatusParams{
		Status:    OrderStatusSuccess,
		OrderCode: int32(orderCode),
	}

	buyerTransaction := db.CreateTransactionParams{
		ID:      uuid.New(),
		Type:    TransactionTypeBuy,
		Status:  TransactionStatusSuccess,
		Amount:  order.Amount,
		UserID:  order.BuyerID,
		OrderID: order.ID,
	}

	sellerTransaction := db.CreateTransactionParams{
		ID:      uuid.New(),
		Type:    TransactionTypeReceive,
		Status:  TransactionStatusSuccess,
		Amount:  order.Amount,
		UserID:  order.SellerID,
		OrderID: order.ID,
	}

	sellerWallet := db.UpdateWalletBalanceParams{
		Amount: int64(math.Ceil(float64(order.Amount) * (1 - app.config.feePercentage))),
		UserID: order.SellerID,
	}

	userDownloaded := db.InsertUserDownloadedArtworkParams{
		UserID:    order.BuyerID,
		ArtworkID: order.ArtworkID,
	}

	err = app.updateOrderStatusTransaction(r.Context(), orderUpdate, sellerWallet, userDownloaded, buyerTransaction, sellerTransaction)
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	redirectURL := app.config.returnAppURL + Transaction + buyerTransaction.ID.String()

	http.Redirect(w, r, redirectURL, http.StatusSeeOther)
}

func (app *application) getOrderByIdHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	orderId, err := app.readIDParam(r, "orderId")
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	_, err = app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	order, err := app.queries.GetOrderById(r.Context(), orderId)
	if err != nil {
		app.notFoundErrorResponse(w, OrderNotFound)
		return
	}

	if order.Status == OrderStatusFailure || order.Status == OrderStatusSuccess {
		order.PaymentLink = pgtype.Text{
			String: "",
			Valid:  false,
		}
	}

	err = payos.Key(app.config.payosClientId, app.config.payosApiKey, app.config.payosChecksumKey)
	if err != nil {
		app.serverErrorResponse(w, err)
	}

	data, err := payos.GetPaymentLinkInformation(strconv.Itoa(int(order.OrderCode)))
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	if data.Status == PaymentLinkExpired {
		order.PaymentLink = pgtype.Text{
			String: "",
			Valid:  false,
		}
		order.Status = OrderStatusFailure
		if err = app.queries.UpdateOrderStatus(r.Context(), db.UpdateOrderStatusParams{
			Status:    OrderStatusFailure,
			OrderCode: order.OrderCode,
		}); err != nil {
			app.serverErrorResponse(w, err)
			return
		}
	}

	if err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          order,
	}, nil); err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) getOrdersHistoryHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	user, err := app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	pageNumber, err := strconv.Atoi(app.readStrings(r.URL.Query(), "page", "1"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	pageSize, err := strconv.Atoi(app.readStrings(r.URL.Query(), "size", "10"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	sortBy := app.readStrings(r.URL.Query(), "sortBy", "status")
	sortOrder := app.readStrings(r.URL.Query(), "sortOrder", "DESC")

	status, err := strconv.Atoi(app.readStrings(r.URL.Query(), "status", "0"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}
	if status < 0 || status > 3 {
		app.badRequestErrorResponse(w, InvalidValue)
		return
	}

	orders, err := app.queries.GetOrdersHistory(r.Context(), db.GetOrdersHistoryParams{
		Size:      int32(pageSize),
		Page:      int32(pageNumber),
		SortBy:    sortBy,
		SortOrder: sortOrder,
		UserID:    user.ID,
		Status:    int16(status),
	})

	if err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          orders,
	}, nil); err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) getOrdersPaginatedHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	//check moderator
	_, err = app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	pageNumber, err := strconv.Atoi(app.readStrings(r.URL.Query(), "page", "1"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	pageSize, err := strconv.Atoi(app.readStrings(r.URL.Query(), "size", "10"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	sortBy := app.readStrings(r.URL.Query(), "sortBy", "status")
	sortOrder := app.readStrings(r.URL.Query(), "sortOrder", "DESC")

	status, err := strconv.Atoi(app.readStrings(r.URL.Query(), "status", "1"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}
	if status < 1 || status > 3 {
		app.badRequestErrorResponse(w, InvalidValue)
		return
	}

	orders, err := app.queries.GetOrdersPaginated(r.Context(), db.GetOrdersPaginatedParams{
		Size:      int32(pageSize),
		Page:      int32(pageNumber),
		SortBy:    sortBy,
		SortOrder: sortOrder,
		Status:    int16(status),
	})

	if err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          orders,
	}, nil); err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) createOrderTransaction(ctx context.Context, order db.CreateOrderParams, user *db.User, postName string) (string, error) {
	tx, err := app.dbPool.Begin(ctx)
	if err != nil {
		return "", fmt.Errorf(FailureToBeginDBTransaction)
	}
	defer func(tx pgx.Tx, ctx context.Context) {
		_ = tx.Rollback(ctx)
	}(tx, ctx)

	qtx := app.queries.WithTx(tx)

	orderCode, err := qtx.CreateOrder(ctx, order)
	if err != nil {
		return "", fmt.Errorf(FailureToCreateOrder, err)
	}

	err = payos.Key(app.config.payosClientId, app.config.payosApiKey, app.config.payosChecksumKey)
	if err != nil {
		return "", fmt.Errorf(FailureToConnectPayOS, err)
	}

	expiredAt := int(time.Now().Add(time.Duration(ExpiredTime) * time.Minute).Unix())

	body := payos.CheckoutRequestType{
		OrderCode:   int64(orderCode),
		Amount:      int(order.Amount),
		Description: TransferContent + strconv.Itoa(int(orderCode)),
		CancelUrl:   app.config.returnServiceURL + CancelURL,
		ReturnUrl:   app.config.returnServiceURL + SuccessURL,
		Signature:   nil,
		Items: []payos.Item{
			{
				Name:     postName,
				Quantity: 1,
				Price:    int(order.Amount),
			},
		},
		BuyerName:    &user.Name,
		BuyerEmail:   nil,
		BuyerPhone:   nil,
		BuyerAddress: nil,
		ExpiredAt:    &expiredAt,
	}

	data, err := payos.CreatePaymentLink(body)
	if err != nil {
		return "", fmt.Errorf(FailureToCreatePaymentLink, err)
	}

	err = qtx.AddPaymentLink(ctx, db.AddPaymentLinkParams{
		PaymentLink: pgtype.Text{
			String: data.CheckoutUrl,
			Valid:  true,
		},
		OrderID: order.ID,
	})
	if err != nil {
		return "", fmt.Errorf(FailureToCreatePaymentLink, err)
	}

	err = tx.Commit(ctx)
	if err != nil {
		return "", fmt.Errorf(FailureToCommitDBTransaction)
	}

	return data.CheckoutUrl, nil
}

func (app *application) updateOrderStatusTransaction(ctx context.Context, order db.UpdateOrderStatusParams,
	sellerWallet db.UpdateWalletBalanceParams, userDownloaded db.InsertUserDownloadedArtworkParams,
	buyerTransaction, sellerTransaction db.CreateTransactionParams) error {

	tx, err := app.dbPool.Begin(ctx)
	if err != nil {
		return fmt.Errorf(FailureToBeginDBTransaction)
	}
	defer func(tx pgx.Tx, ctx context.Context) {
		_ = tx.Rollback(ctx)
	}(tx, ctx)

	qtx := app.queries.WithTx(tx)

	if _, err = qtx.GetWallet(ctx, sellerWallet.UserID); err != nil {
		return fmt.Errorf(FailureToGetSellerWallet, err)
	}

	if err = qtx.UpdateOrderStatus(ctx, order); err != nil {
		return fmt.Errorf(FailureToUpdateOrderStatus, err)
	}
	if order.Status == OrderStatusFailure {
		err = tx.Commit(ctx)
		if err != nil {
			return fmt.Errorf(FailureToCommitDBTransaction)
		}

		return nil
	}

	if _, err = qtx.CreateTransaction(ctx, buyerTransaction); err != nil {
		return fmt.Errorf(FailureToCreateTransaction, err)
	}

	if _, err = qtx.CreateTransaction(ctx, sellerTransaction); err != nil {
		return fmt.Errorf(FailureToCreateTransaction, err)
	}

	if err = qtx.UpdateWalletBalance(ctx, sellerWallet); err != nil {
		return fmt.Errorf(FailureToUpdateBalance, err)
	}

	if err = qtx.InsertUserDownloadedArtwork(ctx, userDownloaded); err != nil {
		return fmt.Errorf(FailureToApproveOrder, err)
	}

	err = tx.Commit(ctx)
	if err != nil {
		return fmt.Errorf(FailureToCommitDBTransaction)
	}

	return nil
}

func getOrderType(input int) int16 {
	switch input {
	case 1:
		return OrderStatusPending
	case 2:
		return OrderStatusSuccess
	case 3:
		return OrderStatusFailure
	default:
		return OrderStatusPending
	}
}
