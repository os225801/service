package main

import (
	"be-omnistroke/internal/pkg/dto/payload"
	"be-omnistroke/internal/pkg/enum"
	"net/http"
)

const (
	WalletNotFound        = "wallet not found!"
	FailureToCreateWallet = "failed to create wallet: %w"
)

func (app *application) getWalletHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	user, err := app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	balance, err := app.queries.GetWallet(r.Context(), user.ID)
	if err != nil {
		app.notFoundErrorResponse(w, WalletNotFound)
		return
	}

	if err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data: map[string]int64{
			"balance": balance,
		},
	}, nil); err != nil {
		app.serverErrorResponse(w, err)
	}
}
