package main

import (
	"be-omnistroke/internal/pkg/dto/payload"
	"be-omnistroke/internal/pkg/enum"
	"net/http"
)

func (app *application) logError(err error, resultCode string, message string) {
	app.logger.PrintError(err, payload.BaseResponse{
		ResultCode:    resultCode,
		ResultMessage: message,
		Data:          nil,
	})
}

func (app *application) errorResponse(writer http.ResponseWriter, status int, data payload.BaseResponse) {
	err := app.writeJson(writer, status, data, nil)
	if err != nil {
		app.logError(err, data.ResultCode, data.ResultMessage)
		writer.WriteHeader(status)
	}
}

func (app *application) badRequestErrorResponse(writer http.ResponseWriter, message string) {
	response := payload.BaseResponse{
		ResultCode:    enum.BadRequestCode,
		ResultMessage: message,
		Data:          nil,
	}

	app.logResponse(http.StatusBadRequest, response)

	response.ResultMessage = enum.BadRequestMessage
	app.errorResponse(writer, http.StatusBadRequest, response)
}

func (app *application) unauthorizedErrorResponse(writer http.ResponseWriter, message string) {
	response := payload.BaseResponse{
		ResultCode:    enum.UnauthorizedCode,
		ResultMessage: message,
		Data:          nil,
	}

	app.logResponse(http.StatusUnauthorized, response)

	response.ResultMessage = enum.UnauthorizedMessage
	app.errorResponse(writer, http.StatusUnauthorized, response)
}

func (app *application) notFoundErrorResponse(writer http.ResponseWriter, message string) {

	response := payload.BaseResponse{
		ResultCode:    enum.NotFoundCode,
		ResultMessage: message,
		Data:          nil,
	}

	app.logResponse(http.StatusUnauthorized, response)

	app.errorResponse(writer, http.StatusNotFound, response)
}

func (app *application) serverErrorResponse(writer http.ResponseWriter, err error) {
	app.logError(err, enum.SystemErrorCode, err.Error())
	response := payload.BaseResponse{
		ResultCode:    enum.SystemErrorCode,
		ResultMessage: enum.SystemErrorMessage,
		Data:          nil,
	}

	app.errorResponse(writer, http.StatusInternalServerError, response)
}

func (app *application) failedValidationResponse(w http.ResponseWriter, errors map[string]string) {
	response := payload.BaseResponse{
		ResultCode:    enum.BadRequestCode,
		ResultMessage: enum.InvalidPayload,
		Data:          map[string]interface{}{"errors": errors},
	}

	app.logResponse(http.StatusBadRequest, response)

	response.Data = nil
	app.errorResponse(w, http.StatusBadRequest, response)
}
