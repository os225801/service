package main

import (
	db "be-omnistroke/db/sqlc"
	"be-omnistroke/internal/pkg/dto/payload"
	"be-omnistroke/internal/pkg/dto/payload/request"
	"be-omnistroke/internal/pkg/enum"
	"be-omnistroke/internal/pkg/validator"
	"github.com/google/uuid"
	"net/http"
	"strconv"
)

const (
	CommentNotFound            = "comment not found!"
	ParentCommentNotFound      = "parent comment not found!"
	ParentCommentBelongsToPost = "parent comment does not belong to post!"
)

func (app *application) createCommentHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	var req request.CreateCommentRequest
	err = app.readJson(w, r, &req)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	v := validator.New()
	if request.ValidateCreateCommentRequest(v, req); !v.Valid() {
		app.failedValidationResponse(w, v.Errors)
		return
	}

	user, err := app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	postId, _ := uuid.Parse(req.Data.PostId)
	if isPostExisted, err := app.queries.IsPostExistedById(r.Context(), postId); err != nil || !isPostExisted {
		app.notFoundErrorResponse(w, PostNotFound)
		return
	}

	var parentCommentId *uuid.UUID

	if req.Data.ParentCommentId != "" {
		tempParentCommentId, _ := uuid.Parse(req.Data.ParentCommentId)
		if isParentCommentExisted, err := app.queries.IsCommentExistedById(r.Context(), tempParentCommentId); err != nil || !isParentCommentExisted {
			app.notFoundErrorResponse(w, ParentCommentNotFound)
			return
		}
		parentCommentId = &tempParentCommentId

		isParentCommentBelongsToPost, err := app.queries.IsCommentBelongsToPost(r.Context(), db.IsCommentBelongsToPostParams{
			ID:     tempParentCommentId,
			PostID: postId,
		})
		if err != nil {
			app.serverErrorResponse(w, err)
			return
		}
		if !isParentCommentBelongsToPost {
			app.badRequestErrorResponse(w, ParentCommentBelongsToPost)
			return
		}
	}

	commentId, err := app.queries.CreateComment(r.Context(), db.CreateCommentParams{
		ID:              uuid.New(),
		Content:         req.Data.Content,
		ParentCommentID: parentCommentId,
		PostID:          postId,
		UserID:          user.ID,
	})
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data: map[string]uuid.UUID{
			"commentId": commentId,
		},
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) deleteCommentHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	user, err := app.getCurrentUser(r)
	if err != nil {
		app.unauthorizedErrorResponse(w, err.Error())
		return
	}

	commentId, err := app.readIDParam(r, "commentId")
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}
	if isCommentExisted, err := app.queries.IsCommentExistedById(r.Context(), commentId); err != nil || !isCommentExisted {
		app.notFoundErrorResponse(w, CommentNotFound)
		return
	}

	err = app.queries.DeleteCommentById(r.Context(), db.DeleteCommentByIdParams{
		ID:     commentId,
		UserID: user.ID,
	})
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          nil,
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) getCommentByIdHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	commentId, err := app.readIDParam(r, "commentId")
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	comment, err := app.queries.GetCommentById(r.Context(), commentId)
	if err != nil {
		app.notFoundErrorResponse(w, CommentNotFound)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          comment,
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) getCommentsByPostHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	postId, err := app.readIDParam(r, "postId")
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	if isPostExisted, err := app.queries.IsPostExistedById(r.Context(), postId); err != nil || !isPostExisted {
		app.notFoundErrorResponse(w, PostNotFound)
		return
	}

	pageNumber, err := strconv.Atoi(app.readStrings(r.URL.Query(), "page", "1"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	pageSize, err := strconv.Atoi(app.readStrings(r.URL.Query(), "size", "10"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	comments, err := app.queries.GetCommentsByPost(r.Context(), db.GetCommentsByPostParams{
		PostID: postId,
		Size:   int32(pageSize),
		Page:   int32(pageNumber),
	})
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          comments,
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) getCommentsByParentCommentHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	parentCommentId, err := app.readIDParam(r, "parentCommentId")
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	if isParentCommentExisted, err := app.queries.IsCommentExistedById(r.Context(), parentCommentId); err != nil || !isParentCommentExisted {
		app.notFoundErrorResponse(w, ParentCommentNotFound)
		return
	}

	pageNumber, err := strconv.Atoi(app.readStrings(r.URL.Query(), "page", "1"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	pageSize, err := strconv.Atoi(app.readStrings(r.URL.Query(), "size", "10"))
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	comments, err := app.queries.GetCommentsByParentComment(r.Context(), db.GetCommentsByParentCommentParams{
		ParentCommentID: parentCommentId,
		Size:            int32(pageSize),
		Page:            int32(pageNumber),
	})
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          comments,
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}

func (app *application) countSubCommentsByIdHandler(w http.ResponseWriter, r *http.Request) {
	err := app.logRequest(r)
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	commentId, err := app.readIDParam(r, "commentId")
	if err != nil {
		app.badRequestErrorResponse(w, err.Error())
		return
	}

	if isCommentExisted, err := app.queries.IsCommentExistedById(r.Context(), commentId); err != nil || !isCommentExisted {
		app.notFoundErrorResponse(w, CommentNotFound)
		return
	}

	count, err := app.queries.CountSubCommentsById(r.Context(), commentId)
	if err != nil {
		app.serverErrorResponse(w, err)
		return
	}

	err = app.writeJson(w, http.StatusOK, payload.BaseResponse{
		ResultCode:    enum.SuccessCode,
		ResultMessage: enum.SuccessMessage,
		Data:          map[string]int{"count": int(count)},
	}, nil)
	if err != nil {
		app.serverErrorResponse(w, err)
	}
}
