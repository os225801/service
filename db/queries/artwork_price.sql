-- name: CreateArtworkPrice :exec
INSERT INTO "artwork_price" (id, price, artwork_id)
VALUES ($1, $2, $3);

-- name: GetArtworkPriceByArtwork :one
SELECT price::bigint FROM "artwork_price"
WHERE artwork_id = $1 AND to_date IS NULL;

-- name: UpdateArtworkPrice :exec
UPDATE "artwork_price"
SET to_date = CURRENT_TIMESTAMP
WHERE artwork_id = $1 AND to_date IS NULL;