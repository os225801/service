-- name: GetSessionByUserId :one
SELECT id, token, expiry_date, user_id FROM auth_user WHERE id = sqlc.arg(id);

-- name: CreateSession :exec
INSERT INTO auth_user(id, token, expiry_date, created_at, role_id, user_id)
VALUES (sqlc.arg(id), sqlc.arg(token), sqlc.arg(expiry_date), sqlc.arg(created_at), sqlc.arg(role_id), sqlc.arg(user_id));

-- name: UpdateSession :exec
UPDATE auth_user SET token = sqlc.arg(token), role_id = sqlc.arg(role_id), expiry_date = sqlc.arg(expiry_date)
WHERE id = sqlc.arg(id);