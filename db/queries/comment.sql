-- name: CreateComment :one
INSERT INTO "comment" (id, content, post_id, user_id, parent_comment_id)
VALUES ($1, $2, $3, $4, NULLIF(@parent_comment_id, '00000000-0000-0000-0000-000000000000')::UUID)
RETURNING id;

-- name: IsCommentExistedById :one
SELECT EXISTS(SELECT 1 FROM "comment" WHERE id = $1) as exist;

-- name: IsCommentBelongsToPost :one
SELECT EXISTS(SELECT 1 FROM "comment" WHERE id = $1 AND post_id = $2) as exist;

-- name: DeleteCommentById :exec
DELETE FROM "comment" WHERE id = $1 AND user_id = $2;

-- name: GetCommentById :one
SELECT c.content, c.created_at, c.post_id,
       json_build_object('userId', c.user_id,
                         'username', u.username,
                         'name', u.name,
                         'avatar', u.avatar_link) AS "user"
    FROM "comment" c
    JOIN "user" u ON c.user_id = u.id
    WHERE c.id = $1;

-- name: GetCommentsByPost :one
WITH filtered_comments AS (
    SELECT
        c.id, c.content, c.created_at AS "createdAt", c.post_id AS "postId",
        json_build_object('userId', c.user_id,
                          'username', u.username,
                          'name', u.name,
                          'avatar', u.avatar_link) AS "user",
        ROW_NUMBER() OVER (ORDER BY c.created_at DESC) AS row_num
    FROM
        "comment" c
            JOIN
        "user" u ON c.user_id = u.id
    WHERE
        c.post_id = $1
    AND c.parent_comment_id IS NULL
),
total_count AS (
    SELECT COUNT(*) AS count FROM "comment" WHERE post_id = $1

)
SELECT
    json_build_object(
            'items', json_agg(filtered_comments.*),
            'totalCount', (SELECT count FROM total_count),
            'hasNextPage', (SELECT count FROM total_count) > (sqlc.arg(size)::INT * sqlc.arg(page)::INT),
            'hasPrevPage', sqlc.arg(page) > 1,
            'totalPages', CEIL(CAST((SELECT count FROM total_count) AS FLOAT)/sqlc.arg(size)),
            'pageNumber', sqlc.arg(page),
            'pageSize', sqlc.arg(size),
            'sortBy', 'createdAt',
            'sortOrder', 'DESC'
    ) AS result
FROM
    filtered_comments
WHERE
    filtered_comments.row_num > ((sqlc.arg(page) - 1) * sqlc.arg(size))
AND filtered_comments.row_num <= (sqlc.arg(page) * sqlc.arg(size));

-- name: GetCommentsByParentComment :one
WITH filtered_comments AS (
    SELECT
        c.id, c.content, c.created_at AS "createdAt", c.post_id AS "postId",
        json_build_object('userId', c.user_id,
                          'username', u.username,
                          'name', u.name,
                          'avatar', u.avatar_link) AS "user",
        ROW_NUMBER() OVER (ORDER BY c.created_at DESC) AS row_num
    FROM
        "comment" c
            JOIN
        "user" u ON c.user_id = u.id
    WHERE
        c.parent_comment_id = $1
), total_count AS (
    SELECT COUNT(*) AS count FROM filtered_comments
)
SELECT
    json_build_object(
            'items', json_agg(filtered_comments.*),
            'totalCount', (SELECT count FROM total_count),
            'hasNextPage', (SELECT count FROM total_count) > (sqlc.arg(size)::INT * sqlc.arg(page)::INT),
            'hasPrevPage', sqlc.arg(page) > 1,
            'totalPages', CEIL(CAST((SELECT count FROM total_count) AS FLOAT)/sqlc.arg(size)),
            'pageNumber', sqlc.arg(page),
            'pageSize', sqlc.arg(size),
            'sortBy', 'createdAt',
            'sortOrder', 'DESC'
    ) AS result
FROM
    filtered_comments
WHERE
    filtered_comments.row_num > ((sqlc.arg(page) - 1) * sqlc.arg(size))
AND filtered_comments.row_num <= (sqlc.arg(page) * sqlc.arg(size));


-- name: CountSubCommentsById :one
SELECT COUNT(*) FROM "comment" WHERE parent_comment_id = $1;