-- name: CountTagsByIds :one
SELECT COUNT(*)
FROM "tag"
WHERE id = ANY(sqlc.arg(tag_ids)::uuid[]);

-- name: GetTagsPaginated :one
WITH filtered_tags AS(
    SELECT id , tag_name AS "tagName", tag_description AS "tagDescription",
       ROW_NUMBER() OVER(ORDER BY tag_name) AS row_num
    FROM "tag" t
    WHERE tag_name ILIKE '%' || sqlc.arg(search_term)::text || '%'
),
total_count AS (
    SELECT COUNT(*) AS count FROM filtered_tags
)
SELECT
    json_build_object(
            'items', json_agg(filtered_tags.* ),
            'totalCount', (SELECT count FROM total_count),
            'hasNextPage', (SELECT count FROM total_count) > (sqlc.arg(size)::INT * sqlc.arg(page)::INT),
            'hasPrevPage', sqlc.arg(page) > 1,
            'totalPages', CEIL(CAST((SELECT count FROM total_count) AS FLOAT)/sqlc.arg(size)),
            'pageNumber', sqlc.arg(page),
            'pageSize', sqlc.arg(size),
            'sortBy', 'tagName',
            'sortOrder', 'ASC'
    ) AS result
FROM
    filtered_tags
WHERE
    filtered_tags.row_num > ((sqlc.arg(page) - 1) * sqlc.arg(size))
  AND filtered_tags.row_num <= (sqlc.arg(page) * sqlc.arg(size));

-- name: GetTagById :one
SELECT id, tag_name, tag_description
FROM "tag"
WHERE id = $1;

-- name: CreateTag :one
INSERT INTO "tag" (id, tag_name, tag_description)
VALUES ($1, $2, $3)
RETURNING id;

-- name: CheckTagExistsByName :one
SELECT EXISTS(SELECT 1 FROM "tag" WHERE tag_name = $1);