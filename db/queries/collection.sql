-- name: CreateCollection :one
INSERT INTO "collection" (id, name, description, user_id)
VALUES (sqlc.arg(id), sqlc.arg(name), sqlc.arg(description), sqlc.arg(user_id))
RETURNING id;

-- name: DeleteCollection :exec
DELETE FROM "collection" WHERE id = sqlc.arg(id) AND user_id = sqlc.arg(user_id);

-- name: IsCollectionExistedById :one
SELECT EXISTS(SELECT 1 FROM "collection" WHERE id = sqlc.arg(id)) as "exists";

-- name: GetCollectionById :one
SELECT id, name, description, user_id FROM "collection"
WHERE id = sqlc.arg(id);

-- name: UpdateCollectionInfo :exec
UPDATE "collection"
SET name = sqlc.arg(name), description = sqlc.arg(description)
WHERE id = sqlc.arg(id) AND user_id = sqlc.arg(user_id);

-- name: GetCollectionsByCurrentUser :many
SELECT c.id, c.name, c.description,
       (SELECT COUNT(*) FROM "collection_post" WHERE collection_id = c.id) as "totalPosts",
       (SELECT a.processed_image_link FROM "artwork" a
            INNER JOIN "collection_post" cp ON a.post_id = cp.post_id
        WHERE cp.collection_id = c.id
        ORDER BY cp.post_id LIMIT 1) AS "coverImage",
       (SELECT EXISTS(SELECT 1 FROM "collection_post" WHERE post_id = sqlc.arg(post_id)::uuid AND collection_id = c.id)) as "status"
FROM "collection" c
WHERE user_id = sqlc.arg(user_id);

-- name: GetCollectionsPaginated :one
WITH filtered_collections AS (
    SELECT c.id, c.name,
           (SELECT json_agg(json_build_object('imageLink', a.processed_image_link))
            FROM "artwork" a
                     INNER JOIN "collection_post" cp ON a.post_id = cp.post_id
            WHERE cp.collection_id = c.id
            GROUP BY cp.collection_id
            LIMIT 4) AS "coverImage",
           ROW_NUMBER() OVER (ORDER BY c.created_at DESC) AS row_num
    FROM "collection" c
    WHERE user_id = sqlc.arg(user_id)
),
     total_count AS (
         SELECT COUNT(*) AS count FROM filtered_collections
     )
SELECT
    json_build_object(
            'items', json_agg(filtered_collections.*),
            'totalCount', (SELECT count FROM total_count),
            'hasNextPage', (SELECT count FROM total_count) > (sqlc.arg(size)::INT * sqlc.arg(page)::INT),
            'hasPrevPage', sqlc.arg(page) > 1,
            'totalPages', CEIL(CAST((SELECT count FROM total_count) AS FLOAT) / sqlc.arg(size)),
            'pageNumber', sqlc.arg(page),
            'pageSize', sqlc.arg(size),
            'sortBy', 'createdAt',
            'sortOrder', 'DESC'
    ) AS result
FROM filtered_collections
WHERE filtered_collections.row_num > ((sqlc.arg(page) - 1) * sqlc.arg(size))
  AND filtered_collections.row_num <= (sqlc.arg(page) * sqlc.arg(size));


-- name: GetCollectionDetails :one
SELECT c.id, c.name AS collection_name, c.description, c.user_id AS "userId", u.name , u.username, u.avatar_link
FROM "collection" c
         INNER JOIN "user" u ON c.user_id = u.id
WHERE c.id = sqlc.arg(collection_id);