-- name: CreateUser :exec
INSERT INTO "user" (id, username, avatar_path, banner_path, avatar_link, banner_link, name, date_of_birth)
VALUES (sqlc.arg(id)::uuid, sqlc.arg(username)::varchar, sqlc.arg(avatar_path)::varchar, sqlc.arg(banner_path)::varchar,
        sqlc.arg(avatar_link)::varchar, sqlc.arg(banner_link)::varchar, sqlc.arg(name)::varchar, sqlc.arg(date_of_birth));

-- name: IsUserExistedById :one
SELECT EXISTS(SELECT 1 FROM "user" WHERE id = $1) as exist;

-- name: IsUserExistedByUsername :one
SELECT EXISTS(SELECT 1 FROM "user" WHERE username = $1) as exist;

-- name: GetUserById :one
SELECT *
FROM "user" WHERE id = $1;

-- name: GetUserInfoById :one
SELECT username, name, date_of_birth, avatar_link, banner_link
FROM "user" WHERE id = $1;

-- name: GetUserByUserName :one
SELECT * FROM "user" WHERE username = sqlc.arg(username);

-- name: UpdateUserInfo :exec
UPDATE "user"
SET name = sqlc.arg(name), date_of_birth = sqlc.arg(date_of_birth), account_number = sqlc.arg(account_number)::varchar,
    bank_name = sqlc.arg(bank_name)::varchar, account_name = sqlc.arg(account_name)::varchar
WHERE id = sqlc.arg(id);

-- name: UpdateUserImage :exec
UPDATE "user"
SET avatar_path = sqlc.arg(avatar_path), banner_path = sqlc.arg(banner_path),
    avatar_link = sqlc.arg(avatar_link), banner_link = sqlc.arg(banner_link)
WHERE id = sqlc.arg(id);

-- name: CountAllUsers :one
SELECT COUNT(*) FROM "user"
WHERE is_ban = FALSE;

-- name: GetProfileInfo :one
SELECT u.id, u.username, u.name, u.avatar_link, u.banner_link,
       ( SELECT COUNT(*)
         FROM "user_followed_user" uf
         WHERE uf.user_id = u.id )
           AS "totalFollowers",
       ( SELECT COUNT(*)
         FROM "user_followed_user" uf
         WHERE uf.follower_id = u.id )
           AS "totalFollowings",
       (EXISTS (SELECT 1
                FROM "user_followed_user" uf
                WHERE uf.user_id = u.id
                  AND uf.follower_id = NULLIF(sqlc.arg(follower_id)::unknown, '00000000-0000-0000-0000-000000000000')::UUID))
           AS "isFollowed"
FROM "user" u WHERE u.id = sqlc.arg(user_id);

-- name: GetCurrentUserProfile :one
SELECT u.id, u.username, u.name, u.avatar_link, u.banner_link,
       (u.account_number)::varchar AS "accountNumber",
       u.bank_name::varchar AS "bankName",
       u.account_name::varchar AS "accountName",
       ( SELECT COUNT(*)
         FROM "user_followed_user" uf
         WHERE uf.user_id = u.id )
           AS "totalFollowers",
       ( SELECT COUNT(*)
         FROM "user_followed_user" uf
         WHERE uf.follower_id = u.id )
           AS "totalFollowings"
FROM "user" u WHERE u.id = sqlc.arg(user_id);

-- name: GetLoggedInUser :one
WITH filtered_auth_users AS (
         SELECT au.id, au.token, au.expiry_date, au.created_at, au.role_id, au.user_id,
                json_build_object('userId', u.id,
                                  'username', u.username,
                                  'name', u.name,
                                  'avatar', u.avatar_link
                                  ) AS "user",
                ROW_NUMBER() OVER (ORDER BY
                    CASE WHEN sqlc.arg(sort_by)::text = 'createdAt' AND sqlc.arg(sort_order)::text = 'ASC' THEN au.created_at END,
                    CASE WHEN sqlc.arg(sort_by)::text = 'createdAt' AND sqlc.arg(sort_order)::text = 'DESC' THEN au.created_at END DESC,
                    CASE WHEN sqlc.arg(sort_by)::text = 'expiryDate' AND sqlc.arg(sort_order)::text = 'ASC' THEN au.expiry_date END,
                    CASE WHEN sqlc.arg(sort_by)::text = 'expiryDate' AND sqlc.arg(sort_order)::text = 'DESC' THEN au.expiry_date END DESC,
                    au.id
                    ) AS row_num
         FROM auth_user au
         INNER JOIN user_role ur on ur.id = au.role_id
         INNER JOIN "user" u on u.id = au.user_id
         WHERE u.username ILIKE '%' || sqlc.arg(search_term)::text || '%' AND au.expiry_date::date >= now()::date

     ),
     total_count AS (
         SELECT COUNT(*) AS count FROM filtered_auth_users
     )
SELECT
    json_build_object(
            'items', json_agg(filtered_auth_users.* ),
            'totalCount', (SELECT count FROM total_count),
            'hasNextPage', (SELECT count FROM total_count) > (sqlc.arg(size)::INT * sqlc.arg(page)::INT),
            'hasPrevPage', sqlc.arg(page) > 1,
            'totalPages', CEIL(CAST((SELECT count FROM total_count) AS FLOAT)/sqlc.arg(size)),
            'pageNumber', sqlc.arg(page),
            'pageSize', sqlc.arg(size),
            'sortBy', sqlc.arg(sort_by),
            'sortOrder', sqlc.arg(sort_order)
    ) AS result
FROM
    filtered_auth_users
WHERE
    filtered_auth_users.row_num > ((sqlc.arg(page) - 1) * sqlc.arg(size))
  AND filtered_auth_users.row_num <= (sqlc.arg(page) * sqlc.arg(size));