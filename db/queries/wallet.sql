-- name: GetWallet :one
SELECT balance FROM wallet WHERE user_id = sqlc.arg(user_id);

-- name: CreateWallet :exec
INSERT INTO wallet (id, user_id, balance)
VALUES (sqlc.arg(id), sqlc.arg(user_id), 0);

-- name: UpdateWalletBalance :exec
UPDATE wallet
SET balance = balance + sqlc.arg(amount)
WHERE user_id = sqlc.arg(user_id);