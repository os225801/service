-- name: LikePost :exec
INSERT INTO "user_liked_post" (user_id, post_id)
VALUES ($1, $2);

-- name: CheckLikeStatus :one
SELECT EXISTS (
    SELECT 1
    FROM "user_liked_post"
    WHERE user_id = $1 AND post_id = $2
);

-- name: UnlikePost :exec
DELETE FROM "user_liked_post"
WHERE user_id = $1 AND post_id = $2;

-- name: CountLikes :one
SELECT COUNT(*)
FROM "user_liked_post"
WHERE post_id = $1;