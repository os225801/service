-- name: InsertArtwork :one
INSERT INTO artwork (
    id,
    image_path,
    processed_image_path,
    image_link,
    processed_image_link,
    type,
    artist_id,
    post_id)
VALUES (sqlc.arg(id), sqlc.arg(image_path)::varchar, sqlc.arg(processed_image_path)::varchar,
        sqlc.arg(image_link)::varchar, sqlc.arg(processed_image_link)::varchar,sqlc.arg(type)::smallint,sqlc.arg(artist_id)::uuid , NULL)
RETURNING id;

-- name: DeleteArtwork :exec
DELETE FROM artwork where id = $1 and artist_id = $2;

-- name: GetArtworkById :one
SELECT id, post_id, artist_id, is_buyable, processed_image_path, image_path, type, processed_image_link, is_deleted
FROM artwork
WHERE id = $1 AND is_deleted = false;

-- name: GetArtworkByIdAndArtist :one
SELECT artist_id, post_id FROM "artwork" WHERE id = $1 AND artist_id = $2 AND is_deleted = false;

-- name: UpdateArtwork :exec
UPDATE "artwork" SET post_id = $2, is_buyable = $3 WHERE id = $1;

-- name: DeleteArtworkSoft :exec
UPDATE "artwork" SET is_deleted = true, image_path = '', processed_image_path = '',
                     is_buyable = false, image_link = '', processed_image_link = ''
WHERE id = $1;

-- name: GetArtworkByPost :one
SELECT id, is_buyable, image_path, processed_image_path FROM artwork
WHERE post_id = $1 AND is_deleted = false;

