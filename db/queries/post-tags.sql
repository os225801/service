-- name: CreatePostTags :exec
INSERT INTO "post_tags" ("post_id", "tag_id")
VALUES ($1, $2);

-- name: DeletePostTags :exec
DELETE FROM "post_tags"
WHERE "post_id" = $1;