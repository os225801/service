-- name: FollowUser :exec
INSERT INTO "user_followed_user" (user_id, follower_id)
VALUES (sqlc.arg(user_id), sqlc.arg(follower_id));

-- name: UnfollowUser :exec
DELETE FROM "user_followed_user"
WHERE user_id = sqlc.arg(user_id) AND follower_id = sqlc.arg(follower_id);

-- name: CheckFollowStatus :one
SELECT EXISTS (
    SELECT 1
    FROM "user_followed_user"
    WHERE user_id = sqlc.arg(user_id) AND follower_id = sqlc.arg(follower_id)
);