-- name: CreateTransaction :one
INSERT INTO "transaction" (id, type, status, amount, user_id, order_id)
VALUES ($1, $2, $3, $4, $5, $6)
RETURNING id;

-- name: WithdrawMoney :exec
INSERT INTO "transaction" (id, type, status, amount, user_id)
VALUES ($1, $2, $3, $4, $5);

-- name: UpdateWithdrawMoneyStatus :exec
UPDATE "transaction" SET status = sqlc.arg(status)::smallint, moderator_id = sqlc.arg(moderator_id)
WHERE id = sqlc.arg(id)
AND user_id = sqlc.arg(user_id)
AND order_id IS NULL ;

-- name: GetTransactionByIdAndUser :one
SELECT id, type, status, transaction_date, amount, user_id
FROM "transaction"
WHERE id = sqlc.arg(id)
AND user_id = sqlc.arg(user_id);

-- name: GetTransactionById :one
SELECT id, type, status, transaction_date, amount, user_id
FROM "transaction"
WHERE id = sqlc.arg(id);

-- name: GetTransactionsHistory :one
WITH filtered_transactions AS (
    SELECT t.id, t.type, t.status, t.amount, t.user_id as "userId", t.order_id as "orderId", t.transaction_date as "transactionDate",
    t.moderator_id,
           ROW_NUMBER() OVER (ORDER BY
               CASE WHEN sqlc.arg(sort_by)::text = 'transactionDate' AND sqlc.arg(sort_order)::text = 'ASC' THEN t.transaction_date END,
               CASE WHEN sqlc.arg(sort_by)::text = 'transactionDate' AND sqlc.arg(sort_order)::text = 'DESC' THEN t.transaction_date END DESC,
               CASE WHEN sqlc.arg(sort_by)::text = 'status' AND sqlc.arg(sort_order)::text = 'ASC' THEN t.status END,
               CASE WHEN sqlc.arg(sort_by)::text = 'status' AND sqlc.arg(sort_order)::text = 'DESC' THEN t.status END DESC,
               CASE WHEN sqlc.arg(sort_by)::text = 'amount' AND sqlc.arg(sort_order)::text = 'ASC' THEN t.amount END,
               CASE WHEN sqlc.arg(sort_by)::text = 'amount' AND sqlc.arg(sort_order)::text = 'DESC' THEN t.amount END DESC,
               CASE WHEN sqlc.arg(sort_by)::text = 'type' AND sqlc.arg(sort_order)::text = 'ASC' THEN t.type END,
               CASE WHEN sqlc.arg(sort_by)::text = 'type' AND sqlc.arg(sort_order)::text = 'DESC' THEN t.type END DESC,
               t.id
               ) AS row_num
    FROM "transaction" t
    WHERE user_id = sqlc.arg(user_id)
      AND (
        (sqlc.arg(keyword) = 'status' AND (t.status = sqlc.arg(value)::smallint OR sqlc.arg(value)::smallint = 0 OR sqlc.arg(value) IS NULL))
            OR
        (sqlc.arg(keyword) = 'type' AND (t.type = sqlc.arg(value)::smallint OR sqlc.arg(value)::smallint = 0 OR sqlc.arg(value) IS NULL))
        )
),
     total_count AS (
         SELECT COUNT(*) AS count FROM filtered_transactions
     )
SELECT
    json_build_object(
            'items', json_agg(filtered_transactions.* ),
            'totalCount', (SELECT count FROM total_count),
            'hasNextPage', (SELECT count FROM total_count) > (sqlc.arg(size)::INT * sqlc.arg(page)::INT),
            'hasPrevPage', sqlc.arg(page) > 1,
            'totalPages', CEIL(CAST((SELECT count FROM total_count) AS FLOAT)/sqlc.arg(size)),
            'pageNumber', sqlc.arg(page),
            'pageSize', sqlc.arg(size),
            'sortBy', sqlc.arg(sort_by),
            'sortOrder', sqlc.arg(sort_order)
    ) AS result
FROM
    filtered_transactions
WHERE
    filtered_transactions.row_num > ((sqlc.arg(page) - 1) * sqlc.arg(size))
  AND filtered_transactions.row_num <= (sqlc.arg(page) * sqlc.arg(size));


-- name: GetTransactionsPaginated :one
WITH filtered_transactions AS (
    SELECT t.id, t.type, t.status, t.amount, t.user_id as "userId", t.order_id as "orderId", t.transaction_date as "transactionDate",
           t.moderator_id,
           ROW_NUMBER() OVER (ORDER BY
               CASE WHEN sqlc.arg(sort_by)::text = 'transactionDate' AND sqlc.arg(sort_order)::text = 'ASC' THEN t.transaction_date END,
               CASE WHEN sqlc.arg(sort_by)::text = 'transactionDate' AND sqlc.arg(sort_order)::text = 'DESC' THEN t.transaction_date END DESC,
               CASE WHEN sqlc.arg(sort_by)::text = 'status' AND sqlc.arg(sort_order)::text = 'ASC' THEN t.status END,
               CASE WHEN sqlc.arg(sort_by)::text = 'status' AND sqlc.arg(sort_order)::text = 'DESC' THEN t.status END DESC,
               CASE WHEN sqlc.arg(sort_by)::text = 'amount' AND sqlc.arg(sort_order)::text = 'ASC' THEN t.amount END,
               CASE WHEN sqlc.arg(sort_by)::text = 'amount' AND sqlc.arg(sort_order)::text = 'DESC' THEN t.amount END DESC,
               CASE WHEN sqlc.arg(sort_by)::text = 'type' AND sqlc.arg(sort_order)::text = 'ASC' THEN t.type END,
               CASE WHEN sqlc.arg(sort_by)::text = 'type' AND sqlc.arg(sort_order)::text = 'DESC' THEN t.type END DESC,
               t.id
               ) AS row_num
    FROM "transaction" t
    WHERE (sqlc.arg(keyword) = 'status' AND t.status = sqlc.arg(value)::smallint)
       OR (sqlc.arg(keyword) = 'type' AND t.type = sqlc.arg(value)::smallint)
),
     total_count AS (
         SELECT COUNT(*) AS count FROM filtered_transactions
     )
SELECT
    json_build_object(
            'items', json_agg(filtered_transactions.* ),
            'totalCount', (SELECT count FROM total_count),
            'hasNextPage', (SELECT count FROM total_count) > (sqlc.arg(size)::INT * sqlc.arg(page)::INT),
            'hasPrevPage', sqlc.arg(page) > 1,
            'totalPages', CEIL(CAST((SELECT count FROM total_count) AS FLOAT)/sqlc.arg(size)),
            'pageNumber', sqlc.arg(page),
            'pageSize', sqlc.arg(size),
            'sortBy', sqlc.arg(sort_by),
            'sortOrder', sqlc.arg(sort_order)
    ) AS result
FROM
    filtered_transactions
WHERE
    filtered_transactions.row_num > ((sqlc.arg(page) - 1) * sqlc.arg(size))
  AND filtered_transactions.row_num <= (sqlc.arg(page) * sqlc.arg(size));