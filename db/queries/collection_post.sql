-- name: AddPostToCollection :exec
INSERT INTO "collection_post" (collection_id, post_id)
VALUES (sqlc.arg(collection_id), sqlc.arg(post_id));

-- name: RemoveAllPostsFromCollection :exec
DELETE FROM "collection_post"
WHERE collection_id = sqlc.arg(collection_id);

-- name: RemovePostFromCollection :exec
DELETE FROM "collection_post"
WHERE collection_id = sqlc.arg(collection_id) AND post_id = sqlc.arg(post_id);

-- name: CheckPostInCollection :one
SELECT EXISTS (
    SELECT 1
    FROM "collection_post"
    WHERE collection_id = sqlc.arg(collection_id) AND post_id = sqlc.arg(post_id)
);