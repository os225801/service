-- name: InsertUserDownloadedArtwork :exec
INSERT INTO user_downloaded_artwork (user_id, artwork_id)
VALUES ($1, $2);

-- name: CheckUserDownloadedArtwork :one
SELECT EXISTS(SELECT 1 FROM user_downloaded_artwork
WHERE user_id = sqlc.arg(user_id) AND artwork_id = sqlc.arg(artwork_id)) as exist;