-- name: GetOrderByArtworkAndBuyer :one
SELECT id, order_code, status, payment_link FROM "order"
WHERE artwork_id = sqlc.arg(artwork_id)
AND buyer_id = sqlc.arg(buyer_id)
ORDER BY order_date DESC
LIMIT 1;

-- name: AddPaymentLink :exec
UPDATE "order" SET payment_link = sqlc.arg(payment_link) WHERE id = sqlc.arg(order_id);

-- name: CountAllOrders :one
SELECT COUNT(*) FROM "order";

-- name: CreateOrder :one
INSERT INTO "order" (id, amount, status, buyer_id, seller_id, artwork_id) VALUES
(sqlc.arg(id), sqlc.arg(amount), sqlc.arg(status), sqlc.arg(buyer_id), sqlc.arg(seller_id),
 sqlc.arg(artwork_id)) RETURNING order_code;

-- name: GetOrderById :one
SELECT o.id, o.order_date, o.amount, o.status, o.buyer_id, o.seller_id, o.artwork_id, o.order_code, o.payment_link,
       a.processed_image_link as "image", p.title as "orderName", p.id as "postId"
FROM "order" o
    INNER JOIN "artwork" a ON a.id = o.artwork_id
    INNER JOIN "post" p ON p.id = a.post_id
WHERE o.id = $1;

-- name: GetOrderByCode :one
SELECT o.id, o.order_date, o.amount, o.status, o.buyer_id, o.seller_id, o.artwork_id
FROM "order" o
WHERE o.order_code = sqlc.arg(order_code);

-- name: GetOrdersHistory :one
WITH filtered_orders AS (
    SELECT o.id, o.amount, o.order_date AS "orderDate", o.status,
           o.buyer_id as "buyerId", o.seller_id as "sellerId",
           o.artwork_id as "artworkId", a.processed_image_link as "image", p.title as "orderName", p.id as "postId",
           ROW_NUMBER() OVER (ORDER BY
               CASE WHEN sqlc.arg(sort_by)::text = 'status' AND sqlc.arg(sort_order)::text = 'ASC' THEN o.status END,
               CASE WHEN sqlc.arg(sort_by)::text = 'status' AND sqlc.arg(sort_order)::text = 'DESC' THEN o.status END DESC,
               CASE WHEN sqlc.arg(sort_by)::text = 'orderDate' AND sqlc.arg(sort_order)::text = 'ASC' THEN o.order_date END,
               CASE WHEN sqlc.arg(sort_by)::text = 'orderDate' AND sqlc.arg(sort_order)::text = 'DESC' THEN o.order_date END DESC,
               CASE WHEN sqlc.arg(sort_by)::text = 'amount' AND sqlc.arg(sort_order)::text = 'ASC' THEN o.amount END,
               CASE WHEN sqlc.arg(sort_by)::text = 'amount' AND sqlc.arg(sort_order)::text = 'DESC' THEN o.amount END DESC,
               o.id
               ) AS row_num
    FROM "order" o
        INNER JOIN "artwork" a ON a.id = o.artwork_id
        INNER JOIN "post" p ON p.id = a.post_id
    WHERE (o.buyer_id = sqlc.arg(user_id) OR o.seller_id = sqlc.arg(user_id))
    AND (o.status = sqlc.arg(status)::smallint OR sqlc.arg(status)::smallint = 0 OR sqlc.arg(status) IS NULL)
),
     total_count AS (
         SELECT COUNT(*) AS count FROM filtered_orders
     )
SELECT
    json_build_object(
            'items', json_agg(filtered_orders.* ),
            'totalCount', (SELECT count FROM total_count),
            'hasNextPage', (SELECT count FROM total_count) > (sqlc.arg(size)::INT * sqlc.arg(page)::INT),
            'hasPrevPage', sqlc.arg(page) > 1,
            'totalPages', CEIL(CAST((SELECT count FROM total_count) AS FLOAT)/sqlc.arg(size)),
            'pageNumber', sqlc.arg(page),
            'pageSize', sqlc.arg(size),
            'sortBy', sqlc.arg(sort_by),
            'sortOrder', sqlc.arg(sort_order)
    ) AS result
FROM
    filtered_orders
WHERE
    filtered_orders.row_num > ((sqlc.arg(page) - 1) * sqlc.arg(size))
  AND filtered_orders.row_num <= (sqlc.arg(page) * sqlc.arg(size));

-- name: GetOrdersPaginated :one
WITH filtered_orders AS (
    SELECT o.id, o.amount, o.order_date AS "orderDate", p.title as "orderName", p.id as "postId",
           o.status, o.buyer_id as "buyerId", o.seller_id as "sellerId",
           o.artwork_id as "artworkId",
           ROW_NUMBER() OVER (ORDER BY
               CASE WHEN sqlc.arg(sort_by)::text = 'status' AND sqlc.arg(sort_order)::text = 'ASC' THEN o.status END,
               CASE WHEN sqlc.arg(sort_by)::text = 'status' AND sqlc.arg(sort_order)::text = 'DESC' THEN o.status END DESC,
               CASE WHEN sqlc.arg(sort_by)::text = 'orderDate' AND sqlc.arg(sort_order)::text = 'ASC' THEN o.order_date END,
               CASE WHEN sqlc.arg(sort_by)::text = 'orderDate' AND sqlc.arg(sort_order)::text = 'DESC' THEN o.order_date END DESC,
               CASE WHEN sqlc.arg(sort_by)::text = 'amount' AND sqlc.arg(sort_order)::text = 'ASC' THEN o.amount END,
               CASE WHEN sqlc.arg(sort_by)::text = 'amount' AND sqlc.arg(sort_order)::text = 'DESC' THEN o.amount END DESC,
               o.id
               ) AS row_num
    FROM "order" o
             INNER JOIN "artwork" a ON a.id = o.artwork_id
             INNER JOIN "post" p ON p.id = a.post_id
    WHERE status = sqlc.arg(status)
),
     total_count AS (
         SELECT COUNT(*) AS count FROM filtered_orders
     )
SELECT
    json_build_object(
            'items', json_agg(filtered_orders.* ),
            'totalCount', (SELECT count FROM total_count),
            'hasNextPage', (SELECT count FROM total_count) > (sqlc.arg(size)::INT * sqlc.arg(page)::INT),
            'hasPrevPage', sqlc.arg(page) > 1,
            'totalPages', CEIL(CAST((SELECT count FROM total_count) AS FLOAT)/sqlc.arg(size)),
            'pageNumber', sqlc.arg(page),
            'pageSize', sqlc.arg(size),
            'sortBy', sqlc.arg(sort_by),
            'sortOrder', sqlc.arg(sort_order)
    ) AS result
FROM
    filtered_orders
WHERE
    filtered_orders.row_num > ((sqlc.arg(page) - 1) * sqlc.arg(size))
  AND filtered_orders.row_num <= (sqlc.arg(page) * sqlc.arg(size));


-- name: UpdateOrderStatus :exec
UPDATE "order" SET status = sqlc.arg(status) WHERE order_code = sqlc.arg(order_code);