// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.26.0
// source: auth_user.sql

package db

import (
	"context"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgtype"
)

const createSession = `-- name: CreateSession :exec
INSERT INTO auth_user(id, token, expiry_date, created_at, role_id, user_id)
VALUES ($1, $2, $3, $4, $5, $6)
`

type CreateSessionParams struct {
	ID         string           `json:"id"`
	Token      pgtype.Text      `json:"token"`
	ExpiryDate pgtype.Timestamp `json:"expiryDate"`
	CreatedAt  pgtype.Timestamp `json:"createdAt"`
	RoleID     pgtype.Int4      `json:"roleId"`
	UserID     uuid.UUID        `json:"userId"`
}

func (q *Queries) CreateSession(ctx context.Context, arg CreateSessionParams) error {
	_, err := q.db.Exec(ctx, createSession,
		arg.ID,
		arg.Token,
		arg.ExpiryDate,
		arg.CreatedAt,
		arg.RoleID,
		arg.UserID,
	)
	return err
}

const getSessionByUserId = `-- name: GetSessionByUserId :one
SELECT id, token, expiry_date, user_id FROM auth_user WHERE id = $1
`

type GetSessionByUserIdRow struct {
	ID         string           `json:"id"`
	Token      pgtype.Text      `json:"token"`
	ExpiryDate pgtype.Timestamp `json:"expiryDate"`
	UserID     uuid.UUID        `json:"userId"`
}

func (q *Queries) GetSessionByUserId(ctx context.Context, id string) (GetSessionByUserIdRow, error) {
	row := q.db.QueryRow(ctx, getSessionByUserId, id)
	var i GetSessionByUserIdRow
	err := row.Scan(
		&i.ID,
		&i.Token,
		&i.ExpiryDate,
		&i.UserID,
	)
	return i, err
}

const updateSession = `-- name: UpdateSession :exec
UPDATE auth_user SET token = $1, role_id = $2, expiry_date = $3
WHERE id = $4
`

type UpdateSessionParams struct {
	Token      pgtype.Text      `json:"token"`
	RoleID     pgtype.Int4      `json:"roleId"`
	ExpiryDate pgtype.Timestamp `json:"expiryDate"`
	ID         string           `json:"id"`
}

func (q *Queries) UpdateSession(ctx context.Context, arg UpdateSessionParams) error {
	_, err := q.db.Exec(ctx, updateSession,
		arg.Token,
		arg.RoleID,
		arg.ExpiryDate,
		arg.ID,
	)
	return err
}
