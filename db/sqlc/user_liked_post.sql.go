// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.26.0
// source: user_liked_post.sql

package db

import (
	"context"

	"github.com/google/uuid"
)

const checkLikeStatus = `-- name: CheckLikeStatus :one
SELECT EXISTS (
    SELECT 1
    FROM "user_liked_post"
    WHERE user_id = $1 AND post_id = $2
)
`

type CheckLikeStatusParams struct {
	UserID uuid.UUID `json:"userId"`
	PostID uuid.UUID `json:"postId"`
}

func (q *Queries) CheckLikeStatus(ctx context.Context, arg CheckLikeStatusParams) (bool, error) {
	row := q.db.QueryRow(ctx, checkLikeStatus, arg.UserID, arg.PostID)
	var exists bool
	err := row.Scan(&exists)
	return exists, err
}

const countLikes = `-- name: CountLikes :one
SELECT COUNT(*)
FROM "user_liked_post"
WHERE post_id = $1
`

func (q *Queries) CountLikes(ctx context.Context, postID uuid.UUID) (int64, error) {
	row := q.db.QueryRow(ctx, countLikes, postID)
	var count int64
	err := row.Scan(&count)
	return count, err
}

const likePost = `-- name: LikePost :exec
INSERT INTO "user_liked_post" (user_id, post_id)
VALUES ($1, $2)
`

type LikePostParams struct {
	UserID uuid.UUID `json:"userId"`
	PostID uuid.UUID `json:"postId"`
}

func (q *Queries) LikePost(ctx context.Context, arg LikePostParams) error {
	_, err := q.db.Exec(ctx, likePost, arg.UserID, arg.PostID)
	return err
}

const unlikePost = `-- name: UnlikePost :exec
DELETE FROM "user_liked_post"
WHERE user_id = $1 AND post_id = $2
`

type UnlikePostParams struct {
	UserID uuid.UUID `json:"userId"`
	PostID uuid.UUID `json:"postId"`
}

func (q *Queries) UnlikePost(ctx context.Context, arg UnlikePostParams) error {
	_, err := q.db.Exec(ctx, unlikePost, arg.UserID, arg.PostID)
	return err
}
