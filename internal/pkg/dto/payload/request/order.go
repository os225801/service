package request

import "be-omnistroke/internal/pkg/validator"

const (
	OrderStatusSuccess = 2
	OrderStatusFailure = 3

	OrderStatusErrorMessage = "order status is invalid!"
)

type CreateOrderRequest struct {
	Data struct {
		ArtworkId string `json:"artworkId"`
	}
}

func ValidateCreateOrderRequest(v *validator.Validator, request CreateOrderRequest) {
	v.Check(validator.Matches(request.Data.ArtworkId, validator.UuidRx), "artworkId", ArtworkIdErrorMessage)
}

type UpdateOrderStatusRequest struct {
	Data struct {
		UserID string `json:"userId"`
		Status int    `json:"status"`
	} `json:"data"`
}

func ValidateUpdateOrderStatusRequest(v *validator.Validator, request UpdateOrderStatusRequest) {
	v.Check(validator.Matches(request.Data.UserID, validator.UuidRx), "userId", UserIdErrorMessage)
	v.Check(request.Data.Status == OrderStatusSuccess || request.Data.Status == OrderStatusFailure, "status", OrderStatusErrorMessage)
}
