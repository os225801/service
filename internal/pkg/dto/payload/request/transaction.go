package request

import "be-omnistroke/internal/pkg/validator"

const (
	TransactionStatusFailure = 1
	TransactionStatusSuccess = 2

	TransactionStatusErrorMessage = "transaction status is invalid! "
	AmountErrorMessage            = "amount is invalid!"
)

type WithdrawTransactionRequest struct {
	Data struct {
		Amount int `json:"amount"`
	} `json:"data"`
}

func ValidateWithdrawTransactionRequest(v *validator.Validator, request WithdrawTransactionRequest) {
	v.Check(request.Data.Amount > 0, "amount", AmountErrorMessage)
}

type UpdateWithdrawMoneyStatusRequest struct {
	Data struct {
		UserId string `json:"userId"`
		Status int    `json:"status"`
	} `json:"data"`
}

func ValidateUpdateWithdrawMoneyStatusRequest(v *validator.Validator, request UpdateWithdrawMoneyStatusRequest) {
	v.Check(validator.Matches(request.Data.UserId, validator.UuidRx), "userId", UserIdErrorMessage)
	v.Check(request.Data.Status == TransactionStatusSuccess || request.Data.Status == TransactionStatusFailure,
		"status", TransactionStatusErrorMessage)
}
