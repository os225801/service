package request

import (
	"be-omnistroke/internal/pkg/validator"
)

type CreateCommentRequest struct {
	Data struct {
		PostId          string `json:"postId"`
		ParentCommentId string `json:"parentCommentId"`
		Content         string `json:"content"`
	} `json:"data"`
}

func ValidateCreateCommentRequest(v *validator.Validator, request CreateCommentRequest) {
	v.Check(validator.Matches(request.Data.PostId, validator.UuidRx), "postId", PostIdErrorMessage)
	if request.Data.ParentCommentId != "" {
		v.Check(validator.Matches(request.Data.ParentCommentId, validator.UuidRx), "parentCommentId", "parent comment id must be uuid!")
	}
	v.Check(request.Data.Content != "", "content", "content must be provided!")
}
