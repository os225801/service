package request

import (
	"be-omnistroke/internal/pkg/validator"
)

type SyncUserRequest struct {
	Data struct {
		AuthId     string `json:"authId"`
		Token      string `json:"token"`
		Username   string `json:"username"`
		Name       string `json:"name"`
		Role       int    `json:"role"`
		ExpiryDate string `json:"expiryDate"`
	} `json:"data"`
}

func ValidateSyncUserRequest(v *validator.Validator, request SyncUserRequest) {
	v.Check(request.Data.AuthId != "", "authId", "authId must be provided!")
	v.Check(request.Data.Token != "", "token", " can't be empty")
	v.Check(request.Data.Username != "", "username", "username must be provided!")
	v.Check(request.Data.Name != "", "name", "name must be provided")
	v.Check(request.Data.ExpiryDate != "", "expiryDate", "must be provided")
}

type InvalidateSessionRequest struct {
	UserId string `json:"userId"`
}

func ValidateInvalidateSessionRequest(v *validator.Validator, request InvalidateSessionRequest) {
	v.Check(request.UserId != "", "userId", "must be provided!")
}
