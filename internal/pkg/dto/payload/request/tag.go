package request

import "be-omnistroke/internal/pkg/validator"

type CreateTagRequest struct {
	Data struct {
		TagName     string `json:"tagName"`
		Description string `json:"description"`
	} `json:"data"`
}

func ValidateCreateTagRequest(v *validator.Validator, request CreateTagRequest) {
	v.Check(request.Data.TagName != "", "tagName", "tag name must be provided!")
}
