package request

import "be-omnistroke/internal/pkg/validator"

type CreateCollectionRequest struct {
	Data struct {
		Name        string `json:"name"`
		Description string `json:"description"`
	} `json:"data"`
}

func ValidateCreateCollectionRequest(v *validator.Validator, request CreateCollectionRequest) {
	v.Check(request.Data.Name != "", "name", "name must be provided!")
}

type UpdateCollectionPostRequest struct {
	Data struct {
		PostId string `json:"postId"`
	} `json:"data"`
}

func ValidateUpdateCollectionPostRequest(v *validator.Validator, request UpdateCollectionPostRequest) {
	v.Check(validator.Matches(request.Data.PostId, validator.UuidRx), "postId", PostIdErrorMessage)
}

type UpdateCollectionInfoRequest struct {
	Data struct {
		Name        string `json:"name"`
		Description string `json:"description"`
	} `json:"data"`
}
