package request

import (
	"be-omnistroke/internal/pkg/validator"
)

const (
	UserIdErrorMessage = "user id must be uuid!"

	AccountNumberErrorMessage = "account number is invalid!"
)

type UpdateUserInfoRequest struct {
	Data struct {
		Name          string `json:"name"`
		DateOfBirth   string `json:"dateOfBirth"`
		AccountNumber string `json:"accountNumber"`
		AccountName   string `json:"accountName"`
		BankName      string `json:"bankName"`
	} `json:"data"`
}

func ValidateUpdateUserInfoRequest(v *validator.Validator, request UpdateUserInfoRequest) {
	if request.Data.AccountNumber != "" {
		v.Check(validator.Matches(request.Data.AccountNumber, validator.BankNumberRx), "accountNumber", AccountNumberErrorMessage)
		v.Check(request.Data.AccountName != "", "accountName", "account name is required!")
		v.Check(request.Data.BankName != "", "bankName", "bank name is required!")
	}
}
