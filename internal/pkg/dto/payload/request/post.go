package request

import (
	"be-omnistroke/internal/pkg/validator"
	"github.com/google/uuid"
)

const (
	PostIdErrorMessage = "post id must be uuid!"
)

type CreatePostRequest struct {
	Data struct {
		Title       string      `json:"title"`
		Description string      `json:"description"`
		Price       int         `json:"price"`
		IsBuyable   bool        `json:"isBuyable"`
		ArtworkId   string      `json:"artworkId"`
		TagIds      []uuid.UUID `json:"tagIds"`
	} `json:"data"`
}

func ValidateCreatePostRequest(v *validator.Validator, request CreatePostRequest) {
	v.Check(request.Data.Title != "", "title", "title must be provided!")
	v.Check(request.Data.Description != "", "description", "description must be provided")
	v.Check(validator.Matches(request.Data.ArtworkId, validator.UuidRx), "artworkId", "artwork id must be uuid!")
	v.Check(request.Data.Price >= 0, "price", "price must be greater than or equal to 0")
	v.Check(len(request.Data.TagIds) > 0, "tagIds", "tag ids must be provided")
}

type UpdatePostRequest struct {
	Data struct {
		Title       string      `json:"title"`
		Description string      `json:"description"`
		IsBuyable   bool        `json:"isBuyable"`
		Price       int         `json:"price"`
		TagIds      []uuid.UUID `json:"tagIds"`
	} `json:"data"`
}

func ValidateUpdatePostRequest(v *validator.Validator, request UpdatePostRequest) {
	v.Check(len(request.Data.TagIds) > 0, "tagIds", "tag ids must be provided")
}
