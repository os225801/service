package enum

const (
	SuccessCode      = "00"
	BadRequestCode   = "01"
	NotFoundCode     = "02"
	UnauthorizedCode = "03"
	SystemErrorCode  = "99"

	SuccessMessage      = "Success"
	BadRequestMessage   = "Bad request"
	SystemErrorMessage  = "System error"
	UnauthorizedMessage = "Unauthorized"

	InvalidPayload = "Invalid payload"
)
