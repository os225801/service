package enum

import "time"

const (
	Bearer = "Bearer"
	Vary   = "Vary"
	Token  = "token"

	InvalidToken         = "invalid token"
	FailureToSignToken   = "failed to sign token: %w"
	FailureToParseToken  = "failed to parse token: %v"
	FailureToParseClaims = "failed to parse claims"

	ApiTokenExpiredTime = time.Hour * 24
)
