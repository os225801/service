package enum

const (
	ProjectDirectory = "uploads"
	ChunkDirectory   = "chunks"
	WebpFile         = "%s.webp"
	JpgFile          = "%s.jpg"

	FailureToGetFile        = "failed to get file"
	FileSizeCannotBeZero    = "file size can not be 0"
	FileSizeTooLarge        = "file size is too large"
	FailureToOpenFile       = "failed to open file: %w"
	FailureToReadBufferFile = "failed to read buffer file data: %w"
	FileIsNotImage          = "file is not an image file"
	FailureToDeleteFile     = "failed to delete %s file"
	FailureToReadFile       = "failed to read file: %w"
	FailureToCopyFile       = "failed to copy file: %w"
	FailureToCheckFile      = "error checking file %s: %v"

	FailureToConvertImage       = "failed to convert image to webp: %w"
	FailureToSaveImage          = "failed to save image: %w"
	FailureToCreateImage        = "failed to create image"
	FailureToAddWatermark       = "failed to add watermark"
	FailureToSaveConvertedImage = "failed to save converted image: %w"

	FailureToCreateDir     = "failed to create directory: %w"
	FailureToGetProjectDir = "error getting project directory: %v"
)
