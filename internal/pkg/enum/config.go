package enum

const (
	EnvFile          = ".env"
	AppPort          = "APP_PORT"
	AppDomain        = "APP_DOMAIN"
	AppImageDomain   = "APP_IMAGE_DOMAIN"
	AppDefaultBanner = "APP_DEFAULT_BANNER"
	AppDefaultAvatar = "APP_DEFAULT_AVATAR"
	DbDsn            = "DB_DSN"
	PayosApiKey      = "PAYOS_API_KEY"
	PayosClientId    = "PAYOS_CLIENT_ID"
	PayosChecksumKey = "PAYOS_CHECKSUM_KEY"
	ReturnServiceURL = "RETURN_SERVICE_URL"
	ReturnAppURL     = "RETURN_APP_URL"
	FeePercentage    = "FEE_PERCENTAGE"
	SecretKey        = "SECRET_KEY"
)
