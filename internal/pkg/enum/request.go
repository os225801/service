package enum

const (
	JsonUnknownField             = "json: unknown field"
	IncorrectJsonTypeForField    = "body contains incorrect JSON type for field %q"
	IncorrectJsonTypeAtCharacter = "body contains incorrect JSON type (at character %d)"
	BadlyFormedJsonAtCharacter   = "body contains badly-formed JSON at (character %d)"
	BadlyFormedJson              = "body contains badly-formed JSON"
	BodyMustNotBeEmpty           = "body must not be empty"
	BodyContainsUnknownKey       = "body contains unknown key %s"
	RequestBodyTooLarge          = "http: request body too large"
	BodyTooLargeMessage          = "body must not be larger than %d bytes"
	BodyContainSingleJsonValue   = "body must contain a single JSON value"
)
