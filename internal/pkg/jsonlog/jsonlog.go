package jsonlog

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"runtime/debug"
	"sync"
	"time"
)

type Level int8

const (
	LevelInfo Level = iota
	LevelError
	LevelFatal
)

const (
	ColorReset    = "\033[0m"
	ColorBlue     = "\033[34m"
	ColorRed      = "\033[31m"
	ColorLightRed = "\033[91m"
)

func (lv Level) String() string {
	switch lv {
	case LevelInfo:
		return "INFO"
	case LevelError:
		return "ERROR"
	case LevelFatal:
		return "FATAL"
	default:
		return ""
	}
}

func (lv Level) Color() string {
	switch lv {
	case LevelInfo:
		return ColorBlue
	case LevelError:
		return ColorLightRed
	case LevelFatal:
		return ColorRed
	default:
		return ColorReset
	}
}

type Logger struct {
	out      io.Writer
	minLevel Level
	mu       sync.Mutex
}

func NewLogger(out io.Writer, minLevel Level) *Logger {
	return &Logger{
		out:      out,
		minLevel: minLevel,
	}
}

func (l *Logger) PrintInfo(message string, properties interface{}) {
	_, _ = l.print(LevelInfo, message, properties)
}

func (l *Logger) PrintError(err error, properties interface{}) {
	_, _ = l.print(LevelError, err.Error(), properties)
}

func (l *Logger) PrintFatal(err error, properties interface{}) {
	_, _ = l.print(LevelFatal, err.Error(), properties)
	os.Exit(1)
}

func (l *Logger) print(level Level, message string, properties interface{}) (int, error) {
	if level < l.minLevel {
		return 0, nil
	}

	aux := struct {
		Level      string      `json:"level"`
		Time       string      `json:"time"`
		Message    string      `json:"message"`
		Properties interface{} `json:"properties,omitempty"`
		Trace      string      `json:"trace,omitempty"`
	}{
		Level:      level.String(),
		Time:       time.Now().UTC().Format(time.RFC3339),
		Message:    message,
		Properties: properties,
	}

	if level >= LevelError {
		aux.Trace = string(debug.Stack())
	}

	var line []byte

	line, err := json.Marshal(aux)
	if err != nil {
		line = []byte(LevelError.String() + ": unable to marshal log message:" + err.Error())
	}

	coloredLine := string(line)
	switch level {
	case LevelError:
		coloredLine = fmt.Sprintf("%sERROR%s", ColorLightRed, ColorReset)
		coloredLine = string(line[:10]) + coloredLine + string(line[15:])
	case LevelFatal:
		coloredLine = fmt.Sprintf("%sFATAL%s", ColorRed, ColorReset)
		coloredLine = string(line[:10]) + coloredLine + string(line[15:])
	default:
		coloredLine = fmt.Sprintf("%sINFO%s", ColorBlue, ColorReset)
		coloredLine = string(line[:10]) + coloredLine + string(line[14:])
	}

	l.mu.Lock()
	defer l.mu.Unlock()

	return l.out.Write(append([]byte(coloredLine + "\n\n")))
}

func (l *Logger) Write(message []byte) (n int, err error) {
	return l.print(LevelError, string(message), nil)
}
