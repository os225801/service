package token

import (
	"be-omnistroke/internal/pkg/enum"
	"fmt"
	"github.com/golang-jwt/jwt/v5"
)

type Claims struct {
	jwt.RegisteredClaims
}

func (c Claims) CreateToken(key string) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, c)

	tokenString, err := token.SignedString([]byte(key))
	if err != nil {
		return "", fmt.Errorf(enum.FailureToSignToken, err)
	}

	return tokenString, nil
}

func GetClaims(tokenString, key string) (*Claims, error) {
	token, err := jwt.ParseWithClaims(tokenString, &Claims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(key), nil
	})

	if err != nil {
		return nil, fmt.Errorf(err.Error())
	}
	if !token.Valid {
		return nil, fmt.Errorf(enum.InvalidToken)
	}

	claims, ok := token.Claims.(*Claims)
	if !ok {
		return nil, fmt.Errorf(enum.FailureToParseClaims)
	}

	return claims, nil
}
