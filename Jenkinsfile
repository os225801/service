pipeline {
    agent any
    environment {
        GIT_COMMIT = sh(script: 'git rev-parse --short=8 HEAD', returnStdout: true).trim()
        SERVICE_ENV_PATH = credentials('SERVICE_ENV_PATH')
        DOCKER_USERNAME = credentials('DOCKER_USERNAME')
        DOCKER_PASSWORD = credentials('DOCKER_PASSWORD')
        DOCKER_TAG = "service-${GIT_COMMIT}"
        IMAGE_STORE_PATH = credentials('IMAGE_STORE_PATH')
    }

    post {
        failure {
            updateGitlabCommitStatus name: 'Pipeline Status', state: 'failed'
        }
        success {
            updateGitlabCommitStatus name: 'Pipeline Status', state: 'success'
        }
        aborted {
            updateGitlabCommitStatus name: 'Pipeline Status', state: 'canceled'
        }
    }

    stages {

        stage('Stop running service') {
            steps {
                script {
                    try {
                        updateGitlabCommitStatus name: 'Stop running service', state: 'running'
                        sh  '''
                            docker rm -f omnistroke_service
                            docker rmi --force $(docker images --filter=reference="*/*:service-*" -q)
                        '''
                        updateGitlabCommitStatus name: 'Stop running service', state: 'success'
                    } catch (Exception e) {
                        updateGitlabCommitStatus name: 'Stop running service', state: 'failed'
                        throw e
                    }
                }
            }
        }

        stage('Build and push image') {
            steps {
                script {
                    try {
                        updateGitlabCommitStatus name: 'Build and push image', state: 'running'
                        sh  """
                            cp \${SERVICE_ENV_PATH} .env
                            docker build -t \${DOCKER_USERNAME}/omnistroke:\${DOCKER_TAG} .
                            echo "\${DOCKER_PASSWORD}" | docker login -u \${DOCKER_USERNAME} --password-stdin
                            docker push \${DOCKER_USERNAME}/omnistroke:\${DOCKER_TAG}
                        """
                        updateGitlabCommitStatus name: 'Build and push image', state: 'success'
                    } catch (Exception e) {
                        updateGitlabCommitStatus name: 'Build and push image', state: 'failed'
                        throw e
                    }
                }
            }
        }

        stage('Deploy') {
            steps {
                script {
                    try {
                        updateGitlabCommitStatus name: 'Deploy', state: 'running'
                         sh '''
                            docker run --volume=${IMAGE_STORE_PATH}:/uploads --restart always --name=omnistroke_service -dp 3000:3000 ${DOCKER_USERNAME}/omnistroke:${DOCKER_TAG}
                        '''
                        updateGitlabCommitStatus name: 'Deploy', state: 'success'
                    } catch (Exception e) {
                        updateGitlabCommitStatus name: 'Deploy', state: 'failed'
                        throw e
                    }
                }
            }
        }

    }
}