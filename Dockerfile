# Stage 1: Build stage
FROM golang:1.22.1-alpine AS builder

# Set the working directory
WORKDIR /build

# Install necessary build dependencies
RUN apk --no-cache add vips-dev vips-tools vips-poppler pkgconfig build-base

# Copy and download dependencies
COPY go.mod go.sum ./
RUN go mod download && go mod verify

# Copy the source code and set environments
COPY ./cmd ./cmd
COPY ./db ./db
COPY ./internal ./internal

COPY ./.env ./.env
ENV GO111MODULE=on
ENV GOCACHE=/root/.cache/go-build

# Builds the application as a staticly linked one, to allow it to run on alpine.
RUN --mount=type=cache,target="/root/.cache/go-build" CGO_CFLAGS_ALLOW=-Xpreprocessor GOOS=linux go build -a -installsuffix cgo -o apiserver ./cmd/api

# Stage 2: Final stage
FROM alpine:edge

# Copy the binary from the build stage
COPY --from=builder ["/build/apiserver", "/build/.env" ,"/"]

# Set the timezone and install CA certificates
RUN apk --no-cache add ca-certificates tzdata vips-dev glib

RUN apk --no-cache add ca-certificates tzdata vips-dev glib font-opensans

# Use nonroot user
RUN addgroup -S nonroot \
    && adduser -S nonroot -G nonroot
USER nonroot

# Set the entrypoint command
ENTRYPOINT ["/apiserver"]
EXPOSE 3000